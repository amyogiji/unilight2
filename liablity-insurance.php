<!DOCTYPE html>
<html lang="en">
<?php include 'common/head.php'; ?>

<body>
    <?php include 'common/header.php'; ?>
    <main>
        <div class="serSectionOne productServices">
            <div class="mainHeaderImage">
                <div class="parallax-window" data-parallax="scroll"
                    data-image-src="assets/images/products/liability.jpg">
                </div>

                <!-- <img src="assets/images/products-marine.png" class="headerImage" /> -->
                <div class="divOverlay"></div>
                <!-- <img src="assets/images/servicesOverlay.svg" class="servicesHeaderOverlay"/> -->
                <!-- <h1 class="serviceHeaderTitle">Liability Insurance</h1> -->
            </div>
        </div>

        <div class="serSectionTwo">
            <div class="container-fuild">

                <div class="row">
                    <div class="col-md-9">
                        <div class="container">
                            <div class="row productServiceleft">
                                <div class="col-md-12">
                                    <h4 class="serviceHeaderTitle">Liability Insurance</h4>
                                    <br />
                                    <ul class="breadcrumb">
                                        <li><a href="/">Home ></a></li>
                                        <li><a href="#">Products ></a></li>
                                        <li><a href="liablity-insurance.php">Liability Insurance</a></li>
                                    </ul>
                                    <p class="productSerhead">
                                        Liability Insurance Policies pay for the compensation amount awarded/settled
                                        (including interest) plus defense costs incurred upto limit of liability in
                                        policy which are generally expressed for Any One Accident (AOA) and Any One Year
                                        (AOY). As against this, Statutory Liability Policies have limits as per the
                                        respective Law. Followings are the most common form of Liability insurance
                                        policies being offered by insurers in India:
                                    </p>

                                    <br />
                                    <div class="accordion custAccordion" id="accordionExample">
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="headingOne">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#collapseOne" aria-expanded="true"
                                                    aria-controls="collapseOne">
                                                    Director’s and Officer’s Liability (D&O) Insurance Policy
                                                </button>
                                            </h2>
                                            <div id="collapseOne" class="accordion-collapse collapse show"
                                                aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    D&O Policy covers legal liability of the Directors and Officers of
                                                    an entity, for any wrongful act, error or omission committed by
                                                    them, resulting in legal action being instituted against them by
                                                    specified third parties like customers, competitors, regulatory
                                                    bodies, members of public and shareholders. Legal action brought by
                                                    employees of the Company are usually granted as an optional
                                                    Extension under the Policy, called Employment Practices Liability
                                                    Extension
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="headingTwo">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#collapseTwo" aria-expanded="true"
                                                    aria-controls="collapseTwo">
                                                    CYBER LIABILITY INSURANCE
                                                </button>
                                            </h2>
                                            <div id="collapseTwo" class="accordion-collapse collapse"
                                                aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    Businesses are witnessing an exponential increase in the
                                                    cyber-attacks recently due to remote working, political agendas or
                                                    vendetta, intricate and exposed supply chain management systems and
                                                    extensive data collection activities being performed. Cyber
                                                    Liability Insurance Policy covers specified risks arising due to
                                                    network intrusion by an unauthorized person
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="headingThree">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#collapseThree" aria-expanded="true"
                                                    aria-controls="collapseThree">
                                                    Commercial General Liability (CGL) Insurance
                                                </button>
                                            </h2>
                                            <div id="collapseThree" class="accordion-collapse collapse"
                                                aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    Commercial General Liability Insurance Policy protect businesses
                                                    against liability claims for bodily injury & property damage arising
                                                    out of incidents occur on the premises, operations, products, and
                                                    completed operations.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="headingFour">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#collapseFour" aria-expanded="true"
                                                    aria-controls="collapseFour">
                                                    Employers Liability / Workmen's Compensation (WC) Insurance Policy
                                                </button>
                                            </h2>
                                            <div id="collapseFour" class="accordion-collapse collapse"
                                                aria-labelledby="headingFour" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    Workmen’s Compensation Insurance Policy covers employers in respect
                                                    of liabilities arising under the Employee’s (Workmen’s) Compensation
                                                    Act due to a workman contracting any of the occupational disease (as
                                                    listed out in Schedule III of the WC Act), provided the person has
                                                    been in the exclusive employment of the said employer for a minimum
                                                    period of 6 months.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="headingFour5">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#collapseFour5" aria-expanded="true"
                                                    aria-controls="collapseFour">
                                                    Public Liability Insurance Policy
                                                </button>
                                            </h2>
                                            <div id="collapseFour5" class="accordion-collapse collapse"
                                                aria-labelledby="headingFour5" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    Public Liability Insurance Policy is a statutory insurance designed
                                                    to cover third party liability claims for Industrial and
                                                    Non-Industrial entities protecting against liability arising out of
                                                    accidents at manufacturing premises, storage premises, hotels,
                                                    restaurants, clubs, auditoriums, residences, offices, shops,
                                                    hospitals, educational institutions, exhibitions, stadiums, pandals,
                                                    film studios, circus, zoo etc.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="headingFour6">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#collapseFour6" aria-expanded="true"
                                                    aria-controls="collapseFour">
                                                    Product Liability Insurance Policy
                                                </button>
                                            </h2>
                                            <div id="collapseFour6" class="accordion-collapse collapse"
                                                aria-labelledby="headingFour6" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    Product Liability Insurance Policy covers third party liability
                                                    claims arising out of accidents from use of products defined as any
                                                    tangible asset after it has left the custody or control of the
                                                    Insured, which has been manufactured (or in some cases
                                                    sold/supplied) by the Insured. For the purpose of this Policy,
                                                    Products are categorized under Domestic or Exports, on account of
                                                    differential risk exposure
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="headingFour6">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#collapseFour6" aria-expanded="true"
                                                    aria-controls="collapseFour">
                                                    Professional Indemnity (PI) Insurance Policy
                                                </button>
                                            </h2>
                                            <div id="collapseFour6" class="accordion-collapse collapse"
                                                aria-labelledby="headingFour6" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    Professional Indemnity Insurance Policy Covers professional errors
                                                    and omissions on the part of the Insured, which lead to a loss
                                                    suffered by the Insured’s clients, who can then bring on a
                                                    compensation claim against him. It is also known as the Errors and
                                                    Omissions (E&O) Policy and can be issued to both professional
                                                    individuals and organizations in different categories
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="serviceHightlight">
                                <h1>How are we at Unilight different?</h1>
                                <ul>
                                    <li>How are we at Unilight different? Team of experts with understanding of practices, risks, exposures across industries.</li>
                                    <li>Ability to carry out risk inspections, safety audits and project risk consulting for identification of risks and offering Loss Prevention solutions.</li>
                                    <li>Provide Innovative Insurance Solutions that help in risk mitigation and risk management.</li>
                                    <li>Technical Assistance in communicating Industrial Risk-related information and Risk Reduction/Mitigating Measures to your company’s board, technical committees and/or the (re-) insurance industry.</li>                                   
                                </ul>
                            </div> -->
                    </div>
                    <div class="col-md-3">
                        <?php include 'common/products/products-right.php' ?>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php include 'common/footer.php'; ?>
</body>

</html>