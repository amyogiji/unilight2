<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Unilight" />
    <meta name="google-site-verification" content="wZLE4U22_5ARGaiT-NFllJcsGYJPCBRRz1nqtSw4ejs" />
    <title>Unilight Insurance </title>
    <link rel="icon" href="assets/images/favicon.png" type="image/png" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,600&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css"
        integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css"
        integrity="sha512-yHknP1/AwR+yx26cB1y0cjvQUMvEa2PFzt1c9LlS4pRQ5NOTZFWbhBig+X9G9eYW/8m0/4OXNx8pxJ6z57x0dw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css"
        integrity="sha512-17EgCFERpgZKcm0j0fEq1YCJuyAWdz9KUtv1EjVuaOz8pDnh/0nZxmU6BBXwaaxqoi9PQXnRWqlcDB027hgv9A=="
        crossorigin="ano-nymous" referrerpolicy="no-referrer" />

    <link rel="stylesheet" href="https://unpkg.com/swiper@7/swiper-bundle.min.css" />

    <script src="assets/scripts/jquery.js"></script>
    <script src="assets/scripts/parallax.min.js"></script>

    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="assets/scripts/animate.css" />


    <script>

    </script>
    <style>
    .home-main-slider .slick-dots {
        transform: rotate(-4deg);
        text-align: left;
        overflow: hidden;
        z-index: 0;
    }

    .home-main-slider .slick-dots li {
        width: 7rem;
        height: 5px;
        background: #f4d2d4;
        margin: 0;
        position: relative;
        font-size: 0;
        border-radius: 0;
    }

    .home-main-slider .slick-dots li:first-child {
        background: #cb333a;
    }

    .home-main-slider .slick-dots li.progress,
    .home-main-slider .slick-dots li.slick-active {
        background: #cb333a;
    }

    .home-main-slider .slick-dots li button {
        display: none;
    }

    .service-slider.slick-list .slick-track {
        /* padding-left: 12.5%; */
    }

    .service-slider.slick-list {
        padding: 0 20% 0 0 !important;
    }
    </style>
</head>