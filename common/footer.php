<footer class="bd-footer">
    <div class="container-fuild">
        <div class="row">
            <div class="col-md-7 footerLeft">
                <div class="row">
                    <div class="col-md-7">
                        <img src="assets/images/unlight-logo.svg" alt="" class="d-inline-block footerLogo">
                        <!-- <p class="footerDesc">We feel proud to be one of the leading general insurance companies of
                            India. We have a huge
                            customer base which includes individuals like you, corporates and SMEs.</p> -->
                    </div>
                    <div class="col-md-5">
                        <p></p>
                    </div>
                </div>
                <div class="row contentBottom">
                    <div class="col-md-6">
                        <div class="footerAddress">
                            <p class="addressTitle">Registered & Corporate Office Address</p>
                            <h1>UNILIGHT INSURANCE BROKERS PRIVATE LIMITED</h1>
                            <div class="subAddress">
                                <p> A-301, Hall Mark Business Plaza, Near Gurunanak Hospital, Sant
                                    Dyaneshwar Marg, Bandra East, Mumbai 400051
                                </p>
                                <p class="addTitle">IRDAI License No. 446<br /> Dated Valid till : 06-01-2022<br />
                                    Category of License : Direct and Life Insurance Brokers<br />
                                    Principal Officer : Mr Jeegar Shah Jeegar.Shah@unilight.in<br />
                                    CIN : U66000MH2011PTC223681N</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="footerAddress">
                            <p class="addressTitle">Registered & Corporate Office Address</p>
                            <h1>UNILIGHT REINSURANCE BROKERS PRIVATE LIMITED</h1>
                            <div class="subAddress">

                                <p> A-301, Hall Mark Business Plaza, Near Gurunanak Hospital, Sant
                                    Dyaneshwar Marg, Bandra East, Mumbai 400051
                                </p>
                                <p class="addTitle">
                                    IRDAI License No. 494 <br /> Dated Valid till : 17-07-2023<br />
                                    Category of License : Direct and Life Insurance Brokers<br />
                                    Principal Officer : Mr Bhaw.Dutt bhaw.dutt@unilightre.in<br />
                                    CIN : U66000MH2013PTC249743
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 text-left text-md-start footerRight">
                <div class="row row-cols-2">
                    <div class="col-md-6">
                        <h1 class="footerLinksTitle">About Us</h1>
                        <ul>
                            <li><a href="aboutus.php">About us</a></li>
                            <li><a href="aboutus.php#mission">Vision and Mission</a></li>
                            <li><a href="aboutus.php#bodire">Board Of Directors</a></li>
                            <li><a href="aboutus.php#managementTeam">Management Team</a></li>
                            <li><a href="aboutus.php#csrSection">Corporate SocialResponsibility</a></li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <h1 class="footerLinksTitle">Others</h1>
                        <ul>
                            <li><a href="customer-grievance.php">Grievances</a></li>
                            <li><a href="contactus.php#officeAddress">Offices </a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Sitemap</a></li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <h1 class="footerLinksTitle">Products</h1>
                        <ul>
                            <li><a class="" href="property-insurance.php">Property & Casualty Insurance</a></li>
                            <li><a class="" href="marine-insurance.php">Marine Insurance</a></li>
                            <li><a class="" href="liablity-insurance.php">Liablity Insurance</a></li>
                            <li><a class="" href="miscellaneous-insurance.php">Miscellaneous Insurance</a></li>
                            <li><a class="" href="specie-insurance.php">Soecie Insurance</a></li>
                            <li><a class="" href="employee-insurance.php">Employee's Benefit</a></li>
                            <li><a class="" href="life-insurance.php">Life Insurance</a></li>
                            <li><a class="" href="product-retail-insurance.php">Retail's Insurance</a></li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <h1 class="footerLinksTitle">Services</h1>
                        <ul>
                            <li><a class="" href="commercial-insurance.php">Commercial Insurance</a></li>
                            <li><a class="" href="retail-insurance.php">Retail Insurance</a></li>
                            <li><a class="" href="reinsurance-placement.php">Reinsurance Placement</a></li>
                            <li><a class="" href="claim-management.php">Claim Management</a></li>
                            <li><a class="" href="risk-management.php">Risk Management</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="bottomFooter">
    <ul>
        <li><a href="">Privacy</a></li>
        <li><a href="">Disclaimer</a></li>
        <li><a href="">Copyright 2021</a></li>
    </ul>
</div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"
    integrity="sha512-XtmMtDEcNz2j7ekrtHvOVR4iwwaD6o/FUJe6+Zq+HgcCsk3kj4uSQQR8weQ2QVj1o0Pk6PwYLohm206ZzNfubg=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://unpkg.com/swiper@7/swiper-bundle.min.js"></script>
<script src="assets/scripts/custom.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.min.js"
    integrity="sha384-skAcpIdS7UcVUC05LJ9Dxay8AXcDYfBJqt1CJ85S/CFujBsIzCIv+l9liuYLaMQ/" crossorigin="anonymous">
</script>

<!-- <script>
$("#showForm").click(function() {
    $(".getInTouchFormCont").show();
}); -->
<script src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyAYA_iPTphXp2OrUjn03NRTRlhcB_hbOno"></script>
<script>
$(document).ready(function() {

    // /* $(selector).hover( inFunction, outFunction ) */
    // $('.dropdown').hover(
    //     function() {
    //         $(this).find('ul').css({
    //             "display": "block",
    //             "margin-top": 0
    //         });
    //     },
    //     function() {
    //         $(this).find('ul').css({
    //             "display": "none",
    //             "margin-top": 0
    //         });
    //     }
    // );
    //Preloader
    preloaderFadeOutTime = 700;

    function hidePreloader() {
        var preloader = $('.loaderImg');
        preloader.fadeOut(preloaderFadeOutTime);
    }
    hidePreloader();
});
var cSpeed = 9;
var cWidth = 128;
var cHeight = 14;
var cTotalFrames = 20;
var cFrameWidth = 128;
var cImageSrc = 'assets/images/sprites.gif';

var cImageTimeout = false;
var cIndex = 0;
var cXpos = 0;
var cPreloaderTimeout = false;
var SECONDS_BETWEEN_FRAMES = 0;

function startAnimation() {

    document.getElementById('loaderImage').style.backgroundImage = 'url(' + cImageSrc + ')';
    document.getElementById('loaderImage').style.width = cWidth + 'px';
    document.getElementById('loaderImage').style.height = cHeight + 'px';

    //FPS = Math.round(100/(maxSpeed+2-speed));
    FPS = Math.round(100 / cSpeed);
    SECONDS_BETWEEN_FRAMES = 1 / FPS;

    cPreloaderTimeout = setTimeout('continueAnimation()', SECONDS_BETWEEN_FRAMES / 1000);

}

function continueAnimation() {

    cXpos += cFrameWidth;
    //increase the index so we know which frame of our animation we are currently on
    cIndex += 1;

    //if our cIndex is higher than our total number of frames, we're at the end and should restart
    if (cIndex >= cTotalFrames) {
        cXpos = 0;
        cIndex = 0;
    }

    if (document.getElementById('loaderImage'))
        document.getElementById('loaderImage').style.backgroundPosition = (-cXpos) + 'px 0';

    cPreloaderTimeout = setTimeout('continueAnimation()', SECONDS_BETWEEN_FRAMES * 1000);
}

function stopAnimation() { //stops animation
    clearTimeout(cPreloaderTimeout);
    cPreloaderTimeout = false;
}

function imageLoader(s, fun) //Pre-loads the sprites image
{
    clearTimeout(cImageTimeout);
    cImageTimeout = 0;
    genImage = new Image();
    genImage.onload = function() {
        cImageTimeout = setTimeout(fun, 0)
    };
    genImage.onerror = new Function('alert(\'Could not load the image\')');
    genImage.src = s;
}

//The following code starts the animation
new imageLoader(cImageSrc, 'startAnimation()');
const serviceSlider = $('.service-slider');
serviceSlider.slick({
    dots: false,
    arrows: false,
    slidesToShow: 2.5,
    infinite: false,
    freeMode: true,
    responsive: [{
        breakpoint: 768,
        settings: {
            slidesToShow: 1.5,
            centerMode: false,
            /* set centerMode to false to show complete slide instead of 3 */
            slidesToScroll: 1
        }
    }]
});
serviceSlider.on('wheel', (function(e) {
    e.preventDefault();
    clearTimeout(scroll);
    // scroll = setTimeout(function () { scrollCount = 0; }, 200);
    // if (scrollCount) return 0;
    // scrollCount = 1;
    if (e.originalEvent.deltaY < 0) {
        $(this).slick('slickPrev');
    } else {
        $(this).slick('slickNext');
    }
}));
$(".knowMore").click(function() {
    $('html, body').animate({
        scrollTop: $("#moreServices").offset().top
    }, 100);
});
var swiper = new Swiper(".mySwiperAbout", {
    slidesPerView: 4,
    spaceBetween: 30,
    pagination: {
        el: ".swiper-pagination",
        clickable: true,
    },
});

$("#showForm").click(function() {
    $(".getInTouch").addClass("getInTouchWidth");
    $(this).addClass("formTypeBorder");
    setTimeout(() => {
        $("#form1").addClass("showContactForm");
        $("#form2").removeClass("showContactForm");
        $("#form3").removeClass("showContactForm");
        $("#form4").removeClass("showContactForm");
        $("#showForm2").removeClass("formTypeBorder");
        $("#showForm3").removeClass("formTypeBorder");
        $("#showForm4").removeClass("formTypeBorder");
    }, 500);
});
$("#showForm2").click(function() {
    $(".getInTouch").addClass("getInTouchWidth");
    $(this).addClass("formTypeBorder");
    setTimeout(() => {
        $("#form2").addClass("showContactForm");
        $("#form1").removeClass("showContactForm");
        $("#form3").removeClass("showContactForm");
        $("#form4").removeClass("showContactForm");
        $("#showForm").removeClass("formTypeBorder");
        $("#showForm3").removeClass("formTypeBorder");
        $("#showForm4").removeClass("formTypeBorder");
    }, 500);
});
$("#showForm3").click(function() {
    $(".getInTouch").addClass("getInTouchWidth");
    $(this).addClass("formTypeBorder");
    setTimeout(() => {
        $("#form3").addClass("showContactForm");
        $("#form1").removeClass("showContactForm");
        $("#form2").removeClass("showContactForm");
        $("#form4").removeClass("showContactForm");
        $("#showForm").removeClass("formTypeBorder");
        $("#showForm2").removeClass("formTypeBorder");
        $("#showForm4").removeClass("formTypeBorder");
    }, 500);
});
$("#showForm4").click(function() {
    $(".getInTouch").addClass("getInTouchWidth");
    $(this).addClass("formTypeBorder");
    setTimeout(() => {
        $("#form4").addClass("showContactForm");
        $("#form1").removeClass("showContactForm");
        $("#form3").removeClass("showContactForm");
        $("#form2").removeClass("showContactForm");
        $("#showForm").removeClass("formTypeBorder");
        $("#showForm3").removeClass("formTypeBorder");
        $("#showForm2").removeClass("formTypeBorder");
    }, 500);
});
$("#gobackform").click(function() {
    $(this).removeClass("formTypeBorder");
    $("#showForm").removeClass("formTypeBorder");
    $(".getInTouch").removeClass("getInTouchWidth");
    $("#form4").removeClass("showContactForm");
    $("#form1").removeClass("showContactForm");
    $("#form3").removeClass("showContactForm");
    $("#form2").removeClass("showContactForm");
    $("#showForm3").removeClass("formTypeBorder");
    $("#showForm2").removeClass("formTypeBorder");
    $("#showForm1").removeClass("formTypeBorder");
});
$("#gobackform1").click(function() {
    $(this).removeClass("formTypeBorder");
    $(".getInTouch").removeClass("getInTouchWidth");
    $("#form4").removeClass("showContactForm");
    $("#form1").removeClass("showContactForm");
    $("#form3").removeClass("showContactForm");
    $("#form2").removeClass("showContactForm");
    $("#showForm").removeClass("formTypeBorder");
    $("#showForm3").removeClass("formTypeBorder");
    $("#showForm2").removeClass("formTypeBorder");
    $("#showForm1").removeClass("formTypeBorder");
});
$("#gobackform2").click(function() {
    $(this).removeClass("formTypeBorder");
    $(".getInTouch").removeClass("getInTouchWidth");
    $("#form4").removeClass("showContactForm");
    $("#form1").removeClass("showContactForm");
    $("#form3").removeClass("showContactForm");
    $("#form2").removeClass("showContactForm");
    $("#showForm").removeClass("formTypeBorder");
    $("#showForm3").removeClass("formTypeBorder");
    $("#showForm2").removeClass("formTypeBorder");
    $("#showForm1").removeClass("formTypeBorder");
});
$("#gobackform3").click(function() {
    $(this).removeClass("formTypeBorder");
    $(".getInTouch").removeClass("getInTouchWidth");
    $("#form4").removeClass("showContactForm");
    $("#form1").removeClass("showContactForm");
    $("#form3").removeClass("showContactForm");
    $("#form2").removeClass("showContactForm");
    $("#showForm").removeClass("formTypeBorder");
    $("#showForm3").removeClass("formTypeBorder");
    $("#showForm1").removeClass("formTypeBorder");
});

$(".unilightForm").submit(function(e) {
    console.log("unilightForm");
    e.preventDefault();

    var contact_form = $(this);
    var formData = contact_form.serializeArray(); //;

    var dataObj = {};

    $(formData).each(function(i, field) {
        dataObj[field.name] = field.value;
    });


    // const formURL = contact_form.eq(0).data("base-url");

    $.ajax({
        type: "POST",
        url: 'contact-submit.php',
        data: formData,
        dataType: "json",
        crossDomain: true,

        beforeSend: function() {
            contact_form.find("");
            contact_form.addClass("form-processing");
            contact_form.find(".btn-submit span").text("Processing...");
            contact_form.find(".form-messsage").html("");
        },
        error: function() {
            contact_form.removeClass("form-processing");
            contact_form.find(".btn-submit span").text("Submit");
            contact_form
                .find(".form-messsage")
                .html(
                    '<div class="alert alert-danger"><strong>Error!</strong> Something Went Wrong. You can contact us via email.</div>'
                );
        },
        success: function(response) {
            contact_form.removeClass("form-processing");
            if (response.status == "success") {
                contact_form
                    .find(".form-messsage")
                    .html(
                        '<div class="alert alert-success"><strong>Success!</strong>Thank you for contacting us. We will get back to you soon.</div>'
                    );
                contact_form.get(0).reset();
                contact_form.find(".btn-submit span").text("Submit");
            } else {
                contact_form
                    .find(".form-messsage")
                    .html(
                        "<div class='alert alert-danger'>Something Went Wrong</div>"
                    );
                // contact_form.find('.form-messsage').html('<div class="alert alert-danger"><strong>Error!</strong> Something Went Wrong. You can contact us via email.</div>');
                contact_form.find(".btn-submit span").text("Submit");
            }
        },
    });
});
/* END - CONTACT FORM */

var locations = [
    // ['A-301, Hall Mark Business Plaza, Near Gurunanak Hospital, <br/> Sant Dyaneshwar Marg, Bandra East, Mumbai 400051  <br/> +91 22 2643 2542', 19.060591, 72.851905, 4,'http://maps.google.com/mapfiles/ms/micons/blue-dot.png'],
    ['2 nd Floor , OMM Complex , Plot No : 488 , <br/>Bomikhal, Rasulgarh, Bhubaneswar – 751010 <br/> +91 90409 85762',
        20.286847, 85.856907, 5
    ],
    ['7J, 7thfloor, Shree Krishna Square 2A Grant Lane <br/> , Kolkata: 700012 Landmark: (Inside Shree Krishna Chambers , Bentick Street)<br/>  +91 33 2235 0021',
        22.570819, 88.353798, 3
    ],
    ['1 st Floor, M-2, South Extension Part-II <br/> New Delhi – 110 049  <br/>+91 11 4267 0090', 28.568193,
        77.222794, 2
    ],
    ['H.No- 1/1196/A-B , 2 nd floor, Kakaji Street,<br/> Near Forever fashion shop, Nanpura, Surat – 395001 <br/> +91 87801 42422',
        21.194347, 72.814691, 1
    ],
    ['409, Reliance Magnum, Opposite Kadamba Bus Stand,<br/> Margao, Goa - 403601<br/>+91 98225 86478', 15.287569,
        73.955036, 6
    ],
    [
        'Premise no. 209 OKAY PLUS TOWER  PNO 5  HATHROI GARI GOVT. HOSTEL,AJMER  ROAD, JAIPUR – 302001.<br/> +91 96506 58983',
        26.91703,
        75.79981, 6
    ],
    [
        'C-100, 1st Floor, RDC Rajnagar, Ghaziabad – 201002 <br/>+91 98290 44048', 28.67427, 77.44337, 6
    ]
];

var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 5,
    center: new google.maps.LatLng(19.060591, 72.851905),
    mapTypeId: google.maps.MapTypeId.ROADMAP
});

var infowindow = new google.maps.InfoWindow();

var marker, i;

for (i = 0; i < locations.length; i++) {
    marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
    });

    google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
            infowindow.setContent(locations[i][0]);
            infowindow.open(map, marker);
        }
    })(marker, i));
}
marker = new google.maps.Marker({
    position: new google.maps.LatLng(19.060591, 72.851905),
    map: map,
    icon: {
        url: 'https://maps.google.com/mapfiles/ms/micons/blue-dot.png',
        scaledSize: new google.maps.Size(60, 50)
    }
});

google.maps.event.addListener(marker, 'click', (function(marker, i) {
    return function() {
        infowindow.setContent(
            'A-301, Hall Mark Business Plaza, Near Gurunanak Hospital, <br/> Sant Dyaneshwar Marg, Bandra East, Mumbai 400051  <br/> +91 22 2643 2542'
        );
        infowindow.open(map, marker);
    }
})(marker, i));
</script>


<script src="assets/scripts/wow.min.js"></script>
<script>
new WOW().init();
</script>