<div class="serSectionRight">
    <ul>
        <li><a class="" href="commercial-insurance.php">Commercial Insurance</a></li>
        <li><a class="" href="retail-insurance.php">Retail Insurance</a></li>
        <li><a class="" href="reinsurance-placement.php">Reinsurance Placement</a></li>
        <li><a class="" href="claim-management.php">Claim Management</a></li>
        <li><a class="" href="risk-management.php">Risk Management</a></li>
    </ul>
    <a href="contactus.php" class="btn btn-lg cusbtn mb-3 me-md-3">Get In Touch</a>
</div>