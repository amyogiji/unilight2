<div class="getInTouch">
    <h1 class="sf-title">Get in touch</h1>
    <div style="display: flex;flex-direction: row;">
        <div class="listContentContact">
            <div class="sf-list" id="showForm">
                <img src="assets/images/handshake.svg">
                <h1>Become our Buiness Partner</h1>
                <p>We feel proud to be one of the leading general insurance</p>
            </div>
            <div class="sf-list" id="showForm2">
                <img src="assets/images/Page-1.svg">
                <h1>Get In Touch</h1>
                <p>We feel proud to be one of the leading general insurance</p>
            </div>
            <div class="sf-list" id="showForm3">
                <img src="assets/images/Page-1-1.svg">
                <h1>Intimate A Claim</h1>
                <p>We feel proud to be one of the leading general insurance</p>
            </div>
            <div class="sf-list" id="showForm4">
                <img src="assets/images/Page-11.svg">
                <h1>Become Part Of Our Growing Team</h1>
                <p>We feel proud to be one of the leading general insurance</p>
            </div>
        </div>
        <div class="getInTouchFormCont" id="form1">
            <div class="backBtnContainer">
                <a id="gobackform" class="gobackform">
                    <span>⟵</span>
                    <p>Go Back</p>
                </a>
            </div>

            <h1>Our team has got your requirements covered!</h1>
            <p>Enter your email and our experts will reach out to you.</p>

            <div class="getInTouchForm">
                <form class="unilightForm" method="post">
                    <div class="mb-3">
                        <input type="text" class="form-control" placeholder="Full Name">
                    </div>
                    <div class="mb-3">
                        <input type="text" class="form-control" placeholder="Educational Qualification *">
                    </div>
                    <div class="mb-3">
                        <input type="text" class="form-control" placeholder="Working Location *">
                    </div>
                    <div class="mb-3">
                        <input type="text" class="form-control" placeholder="PAN No *">
                    </div>
                    <div class="mb-3">
                        <input type="text" class="form-control" placeholder="Mobile No *">
                    </div>
                    <div class="mb-3">
                        <input type="text" class="form-control" placeholder="Email">
                    </div>
                    <div class="mb-3">
                        <textarea class="form-control" rows="3" placeholder="Message"></textarea>
                    </div>
                    <div class="mb-3">
                        <button type="submit" class="btn btn-primary mb-3">Request a
                            callback</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="getInTouchFormCont" id="form2">
            <div class="backBtnContainer">
                <a id="gobackform1" class="gobackform">
                    <span>⟵</span>
                    <p>Go Back</p>
                </a>
            </div>

            <h1>Our team has got your requirements covered!</h1>
            <p>Enter your email and our experts will reach out to you.</p>

            <div class="getInTouchForm">
                <div class="mb-3">
                    <input type="email" class="form-control" placeholder="Full Name">
                </div>
                <div class="mb-3">
                    <input type="email" class="form-control" placeholder="Contact No">
                </div>
                <div class="mb-3">
                    <input type="email" class="form-control" placeholder="Email">
                </div>
                <div class="mb-3">
                    <textarea class="form-control" rows="3" placeholder="Message"></textarea>
                </div>
                <div class="mb-3">
                    <button type="submit" class="btn btn-primary mb-3">Request a
                        callback</button>
                </div>
            </div>
        </div>
        <div class="getInTouchFormCont" id="form3">
            <div class="backBtnContainer">
                <a id="gobackform2" class="gobackform">
                    <span>⟵</span>
                    <p>Go Back</p>
                </a>
            </div>

            <h1>Our team has got your requirements covered!</h1>
            <p>Enter your email and our experts will reach out to you.</p>

            <div class="getInTouchForm">
                <div class="mb-3">
                    <input type="text" class="form-control" placeholder="Policy No *">
                </div>
                <div class="mb-3">
                    <input type="text" class="form-control" placeholder="Valid upto">
                </div>
                <div class="mb-3">
                    <input type="text" class="form-control" placeholder="Name of Insured *">
                </div>
                <div class="mb-3">
                    <input type="text" class="form-control" placeholder="Policy Type *">
                </div>
                <div class="mb-3">
                    <input type="text" class="form-control" placeholder="Date of Accident *">
                </div>
                <div class="mb-3">
                    <input type="text" class="form-control" placeholder="Estimated Loss">
                </div>
                <div class="mb-3">
                    <input type="text" class="form-control" placeholder="Cause of Loss *">
                </div>
                <div class="mb-3">
                    <input type="text" class="form-control" placeholder="Contact No *">
                </div>
                <div class="mb-3">
                    <input type="email" class="form-control" placeholder="Email">
                </div>
                <div class="mb-3">
                    <button type="submit" class="btn btn-primary mb-3">Request a
                        callback</button>
                </div>
            </div>
        </div>
        <div class="getInTouchFormCont" id="form4">
            <div class="backBtnContainer">
                <a id="gobackform3" class="gobackform">
                    <span>⟵</span>
                    <p>Go Back</p>
                </a>
            </div>

            <h1>Our team has got your requirements covered!</h1>
            <p>Enter your email and our experts will reach out to you.</p>

            <div class="getInTouchForm">
                <div class="mb-3">
                    <input type="text" class="form-control" placeholder="Full Name">
                </div>
                <div class="mb-3">
                    <input type="text" class="form-control" placeholder="Educational Qualification *">
                </div>
                <div class="mb-3">
                    <input type="text" class="form-control" placeholder="Working Experience *">
                </div>
                <div class="mb-3">
                    <input type="text" class="form-control" placeholder="Mobile No *">
                </div>
                <div class="mb-3">
                    <input type="text" class="form-control" placeholder="Email">
                </div>
                <div class="mb-3">
                    <textarea class="form-control" rows="3" placeholder="Message"></textarea>
                </div>
                <div class="mb-3">
                    <button type="submit" class="btn btn-primary mb-3">Request a
                        callback</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="getInTouch gitMobile">
    <h1 class="sf-title">Get in touch</h1>
    <div style="display: flex;flex-direction: row;">
        <div class="listContentContact">
            <div class="accordion" id="accordionExample">
                <div class="accordion-item">
                    <h2 class="accordion-header" id="headingTwo">

                        <div class="sf-list collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            <img src="assets/images/Page-1.svg">
                            <h1>Get In Touch</h1>
                            <p>We feel proud to be one of the leading general insurance</p>
                        </div>

                    </h2>
                    <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo"
                        data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            <div class="getInTouchFormCont" id="form2">

                                <h1>Our team has got your requirements covered!</h1>
                                <p>Enter your email and our experts will reach out to you.</p>

                                <div class="getInTouchForm">
                                    <div class="mb-3">
                                        <input type="email" class="form-control" placeholder="Full Name">
                                    </div>
                                    <div class="mb-3">
                                        <input type="email" class="form-control" placeholder="Contact No">
                                    </div>
                                    <div class="mb-3">
                                        <input type="email" class="form-control" placeholder="Email">
                                    </div>
                                    <div class="mb-3">
                                        <textarea class="form-control" rows="3" placeholder="Message"></textarea>
                                    </div>
                                    <div class="mb-3">
                                        <button type="submit" class="btn btn-primary mb-3">Request a
                                            callback</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="headingThree">

                        <div class="sf-list collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            <img src="assets/images/Page-1-1.svg">
                            <h1>Intimate A Claim</h1>
                            <p>We feel proud to be one of the leading general insurance</p>
                        </div>

                    </h2>
                    <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree"
                        data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            <div class="getInTouchFormCont" id="form2">

                                <h1>Our team has got your requirements covered!</h1>
                                <p>Enter your email and our experts will reach out to you.</p>

                                <div class="getInTouchForm">
                                    <div class="mb-3">
                                        <input type="text" class="form-control" placeholder="Policy No *">
                                    </div>
                                    <div class="mb-3">
                                        <input type="text" class="form-control" placeholder="Valid upto">
                                    </div>
                                    <div class="mb-3">
                                        <input type="text" class="form-control" placeholder="Name of Insured *">
                                    </div>
                                    <div class="mb-3">
                                        <input type="text" class="form-control" placeholder="Policy Type *">
                                    </div>
                                    <div class="mb-3">
                                        <input type="text" class="form-control" placeholder="Date of Accident *">
                                    </div>
                                    <div class="mb-3">
                                        <input type="text" class="form-control" placeholder="Estimated Loss">
                                    </div>
                                    <div class="mb-3">
                                        <input type="text" class="form-control" placeholder="Cause of Loss *">
                                    </div>
                                    <div class="mb-3">
                                        <input type="text" class="form-control" placeholder="Contact No *">
                                    </div>
                                    <div class="mb-3">
                                        <input type="email" class="form-control" placeholder="Email">
                                    </div>
                                    <div class="mb-3">
                                        <button type="submit" class="btn btn-primary mb-3">Request a
                                            callback</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="headingOne">
                        <div class="sf-list collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                            <img src="assets/images/handshake.svg">
                            <h1>Join us as POSP</h1>
                            <p>We feel proud to be one of the leading general insurance</p>
                        </div>
                    </h2>
                    <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne"
                        data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            <div class="getInTouchFormCont" id="form2">

                                <h1>Our team has got your requirements covered!</h1>
                                <p>Enter your email and our experts will reach out to you.</p>

                                <div class="getInTouchForm">
                                    <div class="mb-3">
                                        <input type="text" class="form-control" placeholder="Full Name">
                                    </div>
                                    <div class="mb-3">
                                        <input type="text" class="form-control"
                                            placeholder="Educational Qualification *">
                                    </div>
                                    <div class="mb-3">
                                        <input type="text" class="form-control" placeholder="Working Location *">
                                    </div>
                                    <div class="mb-3">
                                        <input type="text" class="form-control" placeholder="PAN No *">
                                    </div>
                                    <div class="mb-3">
                                        <input type="text" class="form-control" placeholder="Mobile No *">
                                    </div>
                                    <div class="mb-3">
                                        <input type="text" class="form-control" placeholder="Email">
                                    </div>
                                    <div class="mb-3">
                                        <textarea class="form-control" rows="3" placeholder="Message"></textarea>
                                    </div>
                                    <div class="mb-3">
                                        <button type="submit" class="btn btn-primary mb-3">Request a
                                            callback</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="headingThree">
                        <div class="sf-list collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                            <img src="assets/images/Page-11.svg">
                            <h1>Become Part Of Our Growing Team</h1>
                            <p>We feel proud to be one of the leading general insurance</p>
                        </div>

                    </h2>
                    <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingThree"
                        data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            <div class="getInTouchFormCont" id="form2">

                                <h1>Our team has got your requirements covered!</h1>
                                <p>Enter your email and our experts will reach out to you.</p>

                                <div class="getInTouchForm">
                                    <div class="mb-3">
                                        <input type="text" class="form-control" placeholder="Full Name">
                                    </div>
                                    <div class="mb-3">
                                        <input type="text" class="form-control"
                                            placeholder="Educational Qualification *">
                                    </div>
                                    <div class="mb-3">
                                        <input type="text" class="form-control" placeholder="Working Experience *">
                                    </div>
                                    <div class="mb-3">
                                        <input type="text" class="form-control" placeholder="Mobile No *">
                                    </div>
                                    <div class="mb-3">
                                        <input type="text" class="form-control" placeholder="Email">
                                    </div>
                                    <div class="mb-3">
                                        <textarea class="form-control" rows="3" placeholder="Message"></textarea>
                                    </div>
                                    <div class="mb-3">
                                        <button type="submit" class="btn btn-primary mb-3">Request a
                                            callback</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>