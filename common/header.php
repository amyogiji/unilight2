<div class="loaderImg">
    <div id="loaderImage"></div>
</div>
<nav class="navbar navbar-expand-lg navbar-light fixed-top">
    <div class="container-fluid">
        <a class="navbar-brand" href="/">
            <img src="assets/images/logo.png" alt="" class="d-inline-block align-text-top">
        </a>
        <button class="navbar-toggler collapsed order-first" type="button" data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent" aria-controls="navbarNav" aria-expanded="false"
            aria-label="Toggle navigation">
            <span> </span>
            <span> </span>
            <span> </span>
        </button>
        <!-- <button class="navbar-toggler order-first" type="button" data-toggle="collapse" data-target="#links" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fa fa-bars"></i>
        </button> -->
        <!-- account toggle -->
        <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#account" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fa fa-user"></i>
        </button> -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <!-- <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="#">Home</a>
                </li> -->
                <li class="nav-item">
                    <a class="nav-link" href="aboutus.php">About us</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                        data-bs-toggle="dropdown" aria-expanded="false">Products</a>
                    <div class="dropdown-menu mega-menu" aria-labelledby="navbarDropdown">
                        <div class="row">
                            <div class="col-6">
                                <a class="dropdown-item" href="property-insurance.php">Property & Casualty Insurance</a>
                                <a class="dropdown-item" href="marine-insurance.php">Marine Insurance</a>
                                <a class="dropdown-item" href="liablity-insurance.php">Liablity Insurance</a>
                                <a class="dropdown-item" href="miscellaneous-insurance.php">Miscellaneous Insurance</a>
                            </div>
                            <div class="col-6">
                                <a class="dropdown-item" href="specie-insurance.php">Specie Insurance</a>
                                <a class="dropdown-item" href="employee-insurance.php">Employee's Benefit</a>
                                <a class="dropdown-item" href="life-insurance.php">Life Insurance</a>
                                <a class="dropdown-item" href="product-retail-insurance.php">Retail Insurance</a>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="nav-item dropdown cusDropMenu">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
                        data-bs-toggle="dropdown" aria-expanded="false">
                        Services
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <li><a class="dropdown-item" href="commercial-insurance.php">Commercial Insurance</a></li>
                        <li><a class="dropdown-item" href="retail-insurance.php">Retail Insurance</a></li>
                        <li><a class="dropdown-item" href="reinsurance-placement.php">Reinsurance Placement</a></li>
                        <li><a class="dropdown-item" href="claim-management.php">Claim Management</a></li>
                        <li><a class="dropdown-item" href="risk-management.php">Risk Management</a></li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="customer-grievance.php">customer grievances</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="contactus.php">contact us</a>
                </li>
            </ul>
            <span class="badge">Primary</span>
        </div>
        <div class="d-flex">
            <div class="socialIcons">
                <ul>
                    <li><a href="https://www.facebook.com/Unilight-Reinsurance-Brokers-Pvt-Ltd-921310681276608/"
                            target="_blank"><img src="assets/images/fb.png" /></a>
                    <li><a href="https://www.instagram.com/unilight_insurance/" target="_blank"><img
                                src="assets/images/insta.png" /></a>
                    <li><a href="https://www.linkedin.com/company/unilight-reinsurance-brokers-private-limited/about/"
                            target="_blank"><img src="assets/images/linkedin.png" /></a>
                    <li><a href="https://twitter.com/UnilightIns" target="_blank"><img
                                src="assets/images/twitter-icon.png" /></a>
                </ul>
            </div>
        </div>
    </div>
</nav>