<!DOCTYPE html>
<html lang="en">
<?php include 'common/head.php'; ?>

<body>
    <?php include 'common/header.php'; ?>
    <main>
        <div class="serSectionOne">
            <div class="mainHeaderImage">
                <div class="parallax-window" data-parallax="scroll"
                    data-image-src="assets/images/services/grievance.jpg">
                    <img src="assets/images/servicesOverlay.svg" class="servicesHeaderOverlay" />
                </div>
                <!-- <img src="assets/images/customer-grievance.png" class="headerImage" />
                <img src="assets/images/servicesOverlay.svg" class="servicesHeaderOverlay" /> -->
                <!-- <h1 class="serviceHeaderTitle">Grievance Redressal Procedure</h1> -->
            </div>
        </div>

        <div class="serSectionTwo">
            <div class="container">
                <div class="row customerTextTitle">
                    <div class="col-md-12">
                        <p>Unilight Insurance Brokers Private Limited attaches utmost importance and committed to
                            provide best possible services to its clients. However, if an event, you are not satisfied
                            with its services and wish to lodge a grievance, you can contact head of our business
                            offices in your area who have been designated as Grievance Redressal Officer (GRO).</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="customerGrievancePage">
                            <p>Alternatively, you can contact us out at </p>
                            <br />
                            <p class="cusContact"><span>Toll Free No: 1800 2101 225 </span><br />or write to us
                                at <span>support@unilight.in</span></p>


                            <p>After investigating the matter internally and subsequent closure, we will send our
                                response within a period of 14 days from the date of receipt of the complaint by the
                                Company. In case the resolution is likely to take a longer time, we will inform you of
                                the same through an interim reply.</p>

                            <p class="cusEscalation"><span>Escalation:</span>
                                For lack of response or if the resolution still does not meet your expectations, you can
                                write to the Chief Grievance Officer. 
                            </p>

                            <p class="cusEscalationDetails">
                                Name - K.K. Aggarwal<br />
                                Designation - Director<br />
                                Email - chiefgrievanceofficer@unilight.in<br />
                                <!-- Contact number - 022 - 26432542 / 26421006<br /> -->
                            </p>

                            <p>After examining the matter, we will send you our final response within a period of 14
                                days from the date of receipt of your complaint on this email id.</p>
                        </div>
                        <!-- <p class="serSTOne">
                                Unilight has emerged as market leader in providing reinsurance support to domestic and international insurers both on Facultative and treaty arrangement basis. With the opening up of Insurance Market, India has witnessed entry of large number of private insurers and resultantly insurance market in terms of penetration and volumes has grown exponentially in last decade. Rapid industrialisation and focus on infrastructure development have seen risks growing in size and complexity. New aged service sector and E-commerce business has redefined business models and exposure to business risk. This economic environment change has resulted into increased demand of insurance protection both for insured and insures end.
                            </p>
                            <br/> -->
                        <!-- <p>
                                We are committed to assist our cedants for their requirements across all Lines of Business but with a focus on the following lines.
                            </p>
                            <br/>
                            <div class="serviceHightlight">
                                <h1>How are we at Unilight different?</h1>
                                <ul>
                                    <li>How are we at Unilight different? Our understanding of risks and ability to structure reinsurance programme.</li>
                                    <li>Knowledge and ability to reach specialised reinsurance markets.</li>
                                    <li>Credibility with reinsures and ability to deliver on-time facultative reinsurance support to our cedants at fair cost.</li>
                                    <li>Post placement support in claim follow up and recoveries.</li>
                                </ul>
                            </div> -->
                    </div>
                    <!-- <div class="col-md-4">
                            <?php include 'common/services/services-right.php' ?>
                        </div> -->
                </div>
            </div>
        </div>
    </main>
    <?php include 'common/footer.php'; ?>
</body>

</html>