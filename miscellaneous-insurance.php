<!DOCTYPE html>
<html lang="en">
<?php include 'common/head.php'; ?>

<body>
    <?php include 'common/header.php'; ?>
    <main>
        <div class="serSectionOne productServices">
            <div class="mainHeaderImage">
                <div class="parallax-window" data-parallax="scroll"
                    data-image-src="assets/images/products/miscellaneous.jpg">
                </div>

                <!-- <img src="assets/images/products-marine.png" class="headerImage" /> -->
                <div class="divOverlay"></div>
                <!-- <img src="assets/images/servicesOverlay.svg" class="servicesHeaderOverlay"/> -->
                <!-- <h1 class="serviceHeaderTitle">Miscellaneous Insurance</h1> -->
            </div>
        </div>

        <div class="serSectionTwo">
            <div class="container-fuild">

                <div class="row">
                    <div class="col-md-9">
                        <div class="container">
                            <div class="row productServiceleft">
                                <div class="col-md-12">
                                    <h4 class="serviceHeaderTitle">Miscellaneous Insurance</h4>
                                    <br />
                                    <ul class="breadcrumb">
                                        <li><a href="/">Home ></a></li>
                                        <li><a href="#">Products ></a></li>
                                        <li><a href="miscellaneous-insurance.php">Miscellaneous Insurance</a></li>
                                    </ul>
                                    <p class="productSerhead">Transit Insurance is one of the oldest forms of insurance
                                        Financial risk which are not part of or excluded under assets insurance policies
                                        can be insured under specific insurance policies designed to insure such risks.
                                        These policies are customized to suit the requirement of businesses and are
                                        flexible in coverage and premium. Followings are the most common form of
                                        miscellanous insurance policies being offered by insurers in India:
                                    </p>

                                    <br />
                                    <div class="accordion custAccordion" id="accordionExample">
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="headingOne">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#collapseOne" aria-expanded="true"
                                                    aria-controls="collapseOne">
                                                    Trade Credit Insurance
                                                </button>
                                            </h2>
                                            <div id="collapseOne" class="accordion-collapse collapse show"
                                                aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    Business allowing sales on credits are exposed to the risk of their
                                                    buyers inability to pay or pay their due in time due to various
                                                    factors like bankruptcy, unexpected financial crises, political or
                                                    economic conditions in the buyer’s country etc. Trade Credit
                                                    Insurance provides protection to businesses against failure of its
                                                    buyers to repay their dues or protracted delays in paying their dues
                                                    thus ensures maintenance of cash flow, safeguarding position of the
                                                    seller’s balance sheet and also providing security to the seller’s
                                                    financiers.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="headingTwo">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#collapseTwo" aria-expanded="true"
                                                    aria-controls="collapseTwo">
                                                    Bankers Indemnity Insurance
                                                </button>
                                            </h2>
                                            <div id="collapseTwo" class="accordion-collapse collapse"
                                                aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    Bankers business is primarily to deal in Money and Securities.
                                                    Bankers Indemnity Insurance is a package policy designed to the
                                                    specific needs of Banks which provides a very comprehensive cover
                                                    for the financial assets (money and securities) belonging to or held
                                                    by all categories of banks.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="headingThree">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#collapseThree" aria-expanded="true"
                                                    aria-controls="collapseThree">
                                                    Sports & Events Insurance
                                                </button>
                                            </h2>
                                            <div id="collapseThree" class="accordion-collapse collapse"
                                                aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    Any unforeseen event leading to cancellation of an event may results
                                                    into substantial financial loss in terms of cost incurred and
                                                    expected earnings. Sports and Event Insurance Policy covers risk of
                                                    Damage to premises (Fire and Allied Perils), Property damage, Bodily
                                                    injury, Cancellation of event, advertisement injury etc.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="headingFour">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#collapseFour" aria-expanded="true"
                                                    aria-controls="collapseFour">
                                                    Money / Cash Insurance
                                                </button>
                                            </h2>
                                            <div id="collapseFour" class="accordion-collapse collapse"
                                                aria-labelledby="headingFour" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    Money Insurance Policy provides protection to commercial
                                                    establishments for loss of cash in transit (due to accident or other
                                                    misfortunes like theft, pick-pocketing, snatching and hold-up) and
                                                    cash in premises/safe due to burglary, robbery and dacoity.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="headingFour5">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#collapseFour5" aria-expanded="true"
                                                    aria-controls="collapseFour">
                                                    Fidelity Guarantee Insurance
                                                </button>
                                            </h2>
                                            <div id="collapseFour5" class="accordion-collapse collapse"
                                                aria-labelledby="headingFour5" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    Fidelity Guarantee Insurance Policy indemnifies the Insured (i.e.
                                                    the employer) against direct financial loss of money or goods
                                                    belonging to him, sustained through specified dishonest acts of the
                                                    employee, in the course of his uninterrupted employment with the
                                                    Insured. The dishonest acts which are specifically covered under the
                                                    Policy are fraud, forgery, embezzlement, larceny and default.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="serviceHightlight">
                                <h1>How are we at Unilight different?</h1>
                                <ul>
                                    <li>How are we at Unilight different? Team of experts with understanding of practices, risks, exposures across industries.</li>
                                    <li>Ability to carry out risk inspections, safety audits and project risk consulting for identification of risks and offering Loss Prevention solutions.</li>
                                    <li>Provide Innovative Insurance Solutions that help in risk mitigation and risk management.</li>
                                    <li>Technical Assistance in communicating Industrial Risk-related information and Risk Reduction/Mitigating Measures to your company’s board, technical committees and/or the (re-) insurance industry.</li>                                   
                                </ul>
                            </div> -->
                    </div>
                    <div class="col-md-3">
                        <?php include 'common/products/products-right.php' ?>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php include 'common/footer.php'; ?>
</body>

</html>