<!DOCTYPE html>
<html lang="en">
<?php include 'common/head.php'; ?>

<body>
    <?php include 'common/header.php'; ?>
    <main>
        <div class="serSectionOne productServices">
            <div class="mainHeaderImage">
                <div class="parallax-window" data-parallax="scroll"
                    data-image-src="assets/images/services/retailservices.jpg">
                    <img src="assets/images/servicesOverlay.svg" class="servicesHeaderOverlay" />
                </div>
                <!-- <img src="assets/images/service-retail.png" class="headerImage" /> -->
                <!-- <h1 class="serviceHeaderTitle">Retail Insurance</h1> -->
            </div>
        </div>

        <div class="serSectionTwo sectionOverflow">
            <div class="container-fuild">
                <div class="row">
                    <div class="col-md-9">
                        <div class="container">
                            <div class="row productServiceleft">
                                <div class="col-md-12">
                                    <h4 class="serviceHeaderTitle">Retail Insurance</h4>
                                    <br />
                                    <ul class="breadcrumb">
                                        <li><a href="/">Home ></a></li>
                                        <li><a href="#">Services ></a></li>
                                        <li><a href="retail-insurance.php">Retail Insurance</a></li>
                                    </ul>
                                    <p class="serSTOne">
                                        Unilight integrated online business portal <a href="http://policyongo.com"
                                            target="_blank">policyongo.com</a> provides customers the ease of buying and
                                        our POSP
                                        partners ease of selling retail insurance products Motor, Health, Accident,
                                        Travel,
                                        Householders and Life on real time basis. Our focus in retail is to provide
                                        insurance
                                        protection to populace specially in tier B and C cities in India with affordable
                                        insurance
                                        plan on real time basis and also to provide support in the event of a loss
                                        claim.
                                    </p>
                                    <br />
                                    <div class="serviceHightlight">
                                        <h1>How are we at Unilight different?</h1>
                                        <ul>
                                            <li>Integrated online business portal policyongo.com to provide POS business
                                                partners
                                                ease of buying retail insurance on real time basis.</li>
                                            <li>On line recruitment and training platform in compliance with IRDAI
                                                guidelines.</li>
                                            <li>Dedicated service team to support business partners in loss claims.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />
                    </div>
                    <div class="col-md-3">
                        <?php include 'common/services/services-right.php' ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="sectionThree mobileSec">
            <div class="container-fluid no-padding">
                <div class="row">
                    <div class="col-md-6 wow fadeInLeft" data-wow-delay="0.5s">
                        <img src="assets/images/Bitmap-4.png" class="homeLaptop" />
                    </div>
                    <div class="col-md-6 wow fadeInRight" data-wow-delay="0.5s">
                        <div class="sthreeRight">
                            <img src="assets/images/Asset 2.png" />
                            <h1>Creating convenience with great price <br /><span>Motor, Health and Life
                                    Insurance</span>
                            </h1>
                            <p>Policyongo.com is digital asset of Unilight Insurance Brokers for distribution of
                                retail
                                insurance products on-line to its customers and through POSP distribution model
                                insurance.
                            </p>
                            <a href="https://policyongo.com/" target="_blank"
                                class="btn btn-lg cusbtn mb-3 me-md-3">Visit
                                Website</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php include 'common/footer.php'; ?>
</body>

</html>