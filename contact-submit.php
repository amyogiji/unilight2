<?php

/* ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
*/

require './PHPMailer/PHPMailerAutoload.php';


header('Content-Type: application/json');


function build_email_template($data, $fileName)
{
    $email_template = file_get_contents('template/'.$fileName, true);

    foreach ($data as $key => $value) {
        $email_template = str_replace('{{' . $key . '}}', $value, $email_template);
    }
    return $email_template;
}

//send type

$data = [
    'Enquiry' => isset($_POST['selector']) && $_POST['selector'] == 1 ? 'Product Enquiry' : 'Dealer Enquiry',
    'Name' => isset($_POST['name']) ? $_POST['name'] : '',
    'Email' => isset($_POST['email']) ? $_POST['email'] : '',
    'Mobile' => $mobileNumber ? $mobileNumber : '',
    'Category' => isset($_POST['category']) ? $_POST['category'] : '',
    'State' => isset($_POST['state']) ? $_POST['state'] : '',
    'City' => isset($_POST['city']) ? $_POST['city'] :  '',
    'Message' => isset($_POST['inquiry_message']) ? $_POST['inquiry_message'] : ''
];

$email_template_final = build_email_template($data);

// Instantiation and passing `true` enables exceptions
$isDebug = false;
$mail = new PHPMailer($isDebug);
if($isDebug){
 $mail->SMTPDebug = SMTP::DEBUG_SERVER;
}
 
$mail->isSMTP();
$mail->Host       = '';                   
$mail->SMTPAuth   = false;                                  
$mail->Username   = '';                   
$mail->Password   = '';                              
$mail->SMTPSecure = 'ssl';
$mail->Port       = 465;          


$mail->setFrom('info@unlight.in', 'info');
$mail->addAddress('info@unlight.in');
$mail->isHTML(true);

$mail->Subject = 'Enquiry Form';
$mail->Body = $email_template_final;

header('Content-Type: application/json');
try {
    if ($mail->send()) {
        echo json_encode(array(
            "is_sent" => true,
            "message" => "Thank you for contacting us. We will get back to you soon."
        ));
    } else {
        echo json_encode(array(
            "is_sent" => false,
            "message" => 'Something Went Wrong' . $isDebug ? $mail->ErrorInfo : ''
        ));
    }
} catch (Exception $e) {
    echo json_encode(array(
        "is_sent" => false,
        "message" => 'Something Went Wrong' . $isDebug ? $mail->ErrorInfo : ''
    ));
}