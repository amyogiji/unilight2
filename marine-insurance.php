<!DOCTYPE html>
<html lang="en">
<?php include 'common/head.php'; ?>

<body>
    <?php include 'common/header.php'; ?>
    <main>
        <div class="serSectionOne productServices">
            <div class="mainHeaderImage">
                <div class="parallax-window" data-parallax="scroll" data-image-src="assets/images/products/marine.jpg">
                </div>

                <!-- <img src="assets/images/products-marine.png" class="headerImage" /> -->
                <div class="divOverlay"></div>
                <!-- <img src="assets/images/servicesOverlay.svg" class="servicesHeaderOverlay"/> -->
                <!-- <h1 class="serviceHeaderTitle">Marine Insurance</h1> -->
            </div>
        </div>

        <div class="serSectionTwo">
            <div class="container-fuild">

                <div class="row">
                    <div class="col-md-9">
                        <div class="container">
                            <div class="row productServiceleft">
                                <div class="col-md-12">
                                    <h4 class="serviceHeaderTitle">Marine Insurance</h4>
                                    <br />
                                    <ul class="breadcrumb">
                                        <li><a href="/">Home ></a></li>
                                        <li><a href="#">Products ></a></li>
                                        <li><a href="marine-insurance.php">Marine Insurance</a></li>
                                    </ul>
                                    <p class="productSerhead">Transit Insurance is one of the oldest forms of insurance
                                        which provides
                                        protection to
                                        policyholders against loss or damage to cargo in transit from one location to
                                        another. This
                                        form of insurance also provide protection to water borne moving assets and
                                        associated
                                        liability risks arising out of such operations. Followings are the most common
                                        form of
                                        Liability insurance policies being offered by insurers in India:</p>

                                    <br />
                                    <div class="accordion custAccordion" id="accordionExample">
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="headingOne">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#collapseOne" aria-expanded="true"
                                                    aria-controls="collapseOne">
                                                    Marine Cargo Insurance (Inland)
                                                </button>
                                            </h2>
                                            <div id="collapseOne" class="accordion-collapse collapse show"
                                                aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    The main modes for inland transits are by road and by rail. However,
                                                    some inland transits also take place by inland waterways and by
                                                    post/courier. Marine Cargo Insurance Policy covers loss or damage to
                                                    cargo in transit from one location to another within India. Marine
                                                    cargo policy duration of cover subject to certain conditions is
                                                    operative for the entire duration of the journey without any period
                                                    of insurance. Marine Cargo Insurance (Inland) policy can be Specific
                                                    Policy is issued to cover a single transit or Open Policy for a
                                                    series of consignments which have to be periodically declared
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="headingTwo">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#collapseTwo" aria-expanded="true"
                                                    aria-controls="collapseTwo">
                                                    Marine Cargo Insurance (Overseas)
                                                </button>
                                            </h2>
                                            <div id="collapseTwo" class="accordion-collapse collapse"
                                                aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    Marine Cargo Insurance (Overseas) Policy covers loss or damage to
                                                    cargo in transit from one location to another situated in a
                                                    different country. Incidental inland transits by road, rail, inland
                                                    waterways etc. from seller’s warehouse to discharge port are covered
                                                    under CIF(Designation Port) based Policy. However, transit from
                                                    discharge port to buyer’s warehouse can also be covered under CIF
                                                    based Policy.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="headingThree">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#collapseThree" aria-expanded="true"
                                                    aria-controls="collapseThree">
                                                    Marine Hull Insurance
                                                </button>
                                            </h2>
                                            <div id="collapseThree" class="accordion-collapse collapse"
                                                aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    Hull and machinery insurance provides physical damage protection for
                                                    the ships / vessels and the machinery which is part of it. This
                                                    Policy, also referred to as H&M Policy, covers loss or damage to
                                                    Hull value and machineries/ equipment of the vessel. Hull Insurance
                                                    Policies are generally bifurcated into Major Hull Insurance and
                                                    Sundry Hull Insurance meant for deep sea vessels and inland or
                                                    costal vessels respectively.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="headingFour">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#collapseFour" aria-expanded="true"
                                                    aria-controls="collapseFour">
                                                    Ship Builder’s Risk Insurance
                                                </button>
                                            </h2>
                                            <div id="collapseFour" class="accordion-collapse collapse"
                                                aria-labelledby="headingFour" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    Builder’s Risks insurance Policy is a purchased by ship builders and
                                                    contractors to cover a new vessel building project before the owner
                                                    takes possession. It is important for a yard to be able to protect
                                                    the value of the newbuilds during construction. A Builder’s Risks
                                                    policy covers loss and liability from accidents during the build
                                                    period and continues during sea trials, till ultimate delivery to
                                                    owners.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="headingFour5">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#collapseFour5" aria-expanded="true"
                                                    aria-controls="collapseFour">
                                                    Ship Owner’s Protection and Indemnity Insurance
                                                </button>
                                            </h2>
                                            <div id="collapseFour5" class="accordion-collapse collapse"
                                                aria-labelledby="headingFour5" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    Protection and indemnity (P&I) liability insurance is specifically
                                                    designed to address the unique needs of the marine industry. It
                                                    covers practically all maritime liability risks associated with the
                                                    ownership and operation of a vessel, including third-party risks for
                                                    damage caused to cargo during transit, risks of environmental damage
                                                    such as oil spills and pollution, war, and political risks.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="headingFour6">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#collapseFour6" aria-expanded="true"
                                                    aria-controls="collapseFour">
                                                    Charterer's Legal Liability
                                                </button>
                                            </h2>
                                            <div id="collapseFour6" class="accordion-collapse collapse"
                                                aria-labelledby="headingFour6" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    There are plenty of things that can go wrong when chartering a
                                                    vessel – from lost cargo and hull damage, to passenger, crew or
                                                    third-party injuries. Charterer of a vessel are exposed to numerous
                                                    responsibilities which may turn into costly liabilities. Charterer’s
                                                    Legal Liability Insurance Policy protects against risks either
                                                    directly to third parties or through contractual liabilities and
                                                    indemnities.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="serviceHightlight">
                                <h1>How are we at Unilight different?</h1>
                                <ul>
                                    <li>How are we at Unilight different? Team of experts with understanding of practices, risks, exposures across industries.</li>
                                    <li>Ability to carry out risk inspections, safety audits and project risk consulting for identification of risks and offering Loss Prevention solutions.</li>
                                    <li>Provide Innovative Insurance Solutions that help in risk mitigation and risk management.</li>
                                    <li>Technical Assistance in communicating Industrial Risk-related information and Risk Reduction/Mitigating Measures to your company’s board, technical committees and/or the (re-) insurance industry.</li>                                   
                                </ul>
                            </div> -->
                    </div>
                    <div class="col-md-3">
                        <?php include 'common/products/products-right.php' ?>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php include 'common/footer.php'; ?>
</body>

</html>