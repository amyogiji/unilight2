<!DOCTYPE html>
<html lang="en">
<?php include 'common/head.php'; ?>

<body>
    <?php include 'common/header.php'; ?>
    <main>
        <div class="serSectionOne productServices">
            <div class="mainHeaderImage">
                <div class="parallax-window" data-parallax="scroll"
                    data-image-src="assets/images/services/claimmanagement.jpg">
                    <img src="assets/images/servicesOverlay.svg" class="servicesHeaderOverlay" />
                </div>
                <!-- <img src="assets/images/service-claim.png" class="headerImage" /> -->
                <!-- <h1 class="serviceHeaderTitle">Claim Management</h1> -->
            </div>
        </div>

        <div class="serSectionTwo">
            <div class="container-fuild">
                <div class="row">
                    <div class="col-md-9">
                        <div class="container">
                            <div class="row productServiceleft">
                                <div class="col-md-12">
                                    <h4 class="serviceHeaderTitle">Claim Management</h4>
                                    <br />
                                    <ul class="breadcrumb">
                                        <li><a href="/">Home ></a></li>
                                        <li><a href="#">Services ></a></li>
                                        <li><a href="claim-management.php">Claim Management</a></li>
                                    </ul>
                                    <p class="serSTOne">
                                        Most business entities face significant challenges during the Claim Settlement
                                        process.
                                        Preparing and presenting a claim can be a complex and time-consuming process and
                                        may need
                                        support of an expert.
                                    </p>
                                    <br />
                                    <p>
                                        Unilight is known for handling claims of their customers in most efficient and
                                        professional
                                        manner with timely and right inputs during the course of preparation,
                                        presentation and
                                        settlement. As a broker we have insight of the pain points on the happening of a
                                        claim like
                                        right values for sum insured, add-on covers etc. We try and ensure that such
                                        typical points
                                        are addressed at the policy placement stage itself and ensure claims settlements
                                        are fair
                                        and hassle-free to our customers. Our dedicated claims team gets engaged from
                                        the loss event
                                        itself and proactively helps customers in the entire journey of claim processing
                                        and
                                        settlement.
                                    </p>
                                    <br />
                                    <div class="serviceHightlight">
                                        <h1>How are we at Unilight different?</h1>
                                        <ul>
                                            <li>How are we at Unilight different? Dedicated commercial claims vertical
                                                having
                                                expertise to handle large value claims.</li>
                                            <li>End to end support to customers in loss events.</li>
                                            <li>Domain expertise in handing property damage and business interruption
                                                losses.</li>
                                            <li>Proactive involvement during claim preparation and presentation process
                                                and support
                                                to customers.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />
                    </div>
                    <div class="col-md-3">
                        <?php include 'common/services/services-right.php' ?>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php include 'common/footer.php'; ?>
</body>

</html>