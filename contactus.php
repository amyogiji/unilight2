<!DOCTYPE html>
<html lang="en">
<?php include 'common/head.php'; ?>

<body>
    <?php include 'common/header.php'; ?>
    <main>
        <div class="serSectionOne" style="padding-bottom:0">
            <div class="mainHeaderImage">
                <div class="parallax-window" data-parallax="scroll" data-image-src="assets/images/contactheader.jpg">
                    <img src="assets/images/servicesOverlay.svg" class="servicesHeaderOverlay" />
                </div>
                <!-- <img src="assets/images/contact-header.png" class="headerImage" /> -->
                <!-- <div class="divOverlay"></div> -->
                <!-- <img src="assets/images/servicesOverlay.svg" class="servicesHeaderOverlay" /> -->
                <!-- <h1 class="serviceHeaderTitle">Contact Us</h1> -->
            </div>
        </div>

        <div class="sectionFour contactForm" style="margin:0">
            <div class="pageTitle">
                <div class="col-md-12">
                    <h4 class="serviceHeaderTitle">Contact Us</h4>
                </div>
            </div>
            <div class="container-fluid no-padding">
                <div class="row">
                    <?php include 'common/contactform.php'; ?>
                    <div class="googlemap no-padding">
                        <div id="map" style="width:100%;height:100%"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="sectionFive officeAddress" id="officeAddress">
            <div class="container">
                <div class="row">
                    <!-- <div class="col-md-11">
                        <div class="officeAddress">
                            <div class="row">
                                <h1>Office Address - </h1>
                                <br />
                                <br />
                                <br />
                                <div class="row no-padding">
                                    <div class="col-md-3">
                                        <div class="addressContainer">
                                            <h1>Registered Office</h1>
                                            <p>Cama Chambers,<br />1st Floor, 23, Nagindas Master Raod,<br />Mumbai 400
                                                023</p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="addressContainer">
                                            <h1>Kolkatta Office</h1>
                                            <p>17J, 7thfloor, Shree Krishna Square <br />2A Grant Lane , Kolkata: 700012
                                                <br />Landmark: (Inside Shree Krishna <br />Chambers , Bentick Street)
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="addressContainer">
                                            <h1>Goa Office</h1>
                                            <p>409, Reliance Magnum,<br />Opposite Kadamba Bus Stand,<br />Margao, Goa -
                                                403601</p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="addressContainer">
                                            <h1>Corporate Office</h1>
                                            <p>B-301, Hall Mark Business Plaza,<br />Near Gurunanak Hospital,<br />Sant
                                                Dyaneshwar Marg, <br />Bandra East,<br />Mumbai 400051</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row no-padding">
                                    <div class="col-md-3">
                                        <div class="addressContainer">
                                            <h1>Delhi Office</h1>
                                            <p>1st Floor, M-2, <br />South Extension Part-II,<br />New Delhi – 110 049
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="addressContainer">
                                            <h1>Bhubneshwar Office</h1>
                                            <p>42nd Floor, OMM Complex,<br /> Plot No : 488,<br /> Bomikhal,
                                                Rasulgarh,<br /> Bhubaneswar – 751010</p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="addressContainer">
                                            <h1>Surat Office</h1>
                                            <p>H.No- 1/1196/A-B ,<br /> 2nd floor, Kakaji Street, Near Forever fashion
                                                <br />shop, Nanpura, Surat – 395001
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="addressContainer">
                                            <h1>Jaipur Office</h1>
                                            <p>Premise no. 209<br /> OKAY PLUS TOWER PNO 5 HATHROI GARI GOVT. HOSTEL,
                                                AJMER ROAD, JAIPUR – 302001. +91 96506 58983
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row no-padding">
                                    <div class="col-md-3">
                                        <div class="addressContainer">
                                            <h1>Ghaziabad Office</h1>
                                            <p>C-100,<br /> 1st Floor, RDC Rajnagar, Ghaziabad – 201002.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
        </div>
        </div>
    </main>
    <?php include 'common/footer.php'; ?>
</body>

</html>