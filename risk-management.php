<!DOCTYPE html>
<html lang="en">
<?php include 'common/head.php'; ?>

<body>
    <?php include 'common/header.php'; ?>
    <main>
        <div class="serSectionOne productServices">
            <div class="mainHeaderImage">
                <div class="parallax-window" data-parallax="scroll"
                    data-image-src="assets/images/services/riskmanagement.jpg">
                    <img src="assets/images/servicesOverlay.svg" class="servicesHeaderOverlay" />
                </div>
                <!-- <img src="assets/images/service-risk.png" class="headerImage" /> -->

                <!-- <h1 class="serviceHeaderTitle">Risk Management</h1> -->
            </div>
        </div>

        <div class="serSectionTwo">
            <div class="container-fuild">
                <div class="row">
                    <div class="col-md-9">
                        <div class="container">
                            <div class="row productServiceleft">
                                <div class="col-md-12">
                                    <h4 class="serviceHeaderTitle">Risk Management</h4>
                                    <br />
                                    <ul class="breadcrumb">
                                        <li><a href="/">Home ></a></li>
                                        <li><a href="#">Services ></a></li>
                                        <li><a href="risk-management.php">risk-management.php Management</a></li>
                                    </ul>
                                    <p class="serSTOne">
                                        Unilight offers end-to end risks mitigation solutions to our clients beginning
                                        with risks
                                        identification, risks recognition, risks assessment, peer comparison and risks
                                        mitigation
                                        solutions while keeping an eye on the regulations in force. Clients considers us
                                        as a
                                        valuable partners for their risk management process. We continuously engage with
                                        client and
                                        also provide update on emerging new risks in changed business environment, major
                                        loss events
                                        and insurance solutions available in the market. Our Risk Management service
                                        emphasizes the
                                        practical risk management concepts, rather than technical calculations and
                                        detailed theory
                                        thus making it easier for our customers to understand. </p>
                                    <br />
                                    <p>
                                        Our above approach has resulted us winning the trust of large number of
                                        Corporate clients
                                        and helped us in becoming a leading insurance solution provider in the country.
                                    </p>
                                    <br />
                                    <div class="serviceHightlight">
                                        <h1>How are we at Unilight different?</h1>
                                        <ul>
                                            <li>How are we at Unilight different? Team of experts with understanding of
                                                practices,
                                                risks, exposures across industries.</li>
                                            <li>Ability to carry out risk inspections, safety audits and project risk
                                                consulting for
                                                identification of risks and offering Loss Prevention solutions.</li>
                                            <li>Provide Innovative Insurance Solutions that help in risk mitigation and
                                                risk
                                                management.</li>
                                            <li>Technical Assistance in communicating Industrial Risk-related
                                                information and Risk
                                                Reduction/Mitigating Measures to your company’s board, technical
                                                committees and/or
                                                the (re-) insurance industry.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />
                    </div>
                    <div class="col-md-3">
                        <?php include 'common/services/services-right.php' ?>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php include 'common/footer.php'; ?>
</body>

</html>