<!DOCTYPE html>
<html lang="en">
<?php include 'common/head.php'; ?>

<body>
    <?php include 'common/header.php'; ?>
    <main>
        <div class="serSectionOne" style="padding-bottom:0">
            <div class="mainHeaderImage">
                <div class="parallax-window" data-parallax="scroll" data-image-src="assets/images/aboutus-header.png">
                    <img src="assets/images/servicesOverlay.svg" class="servicesHeaderOverlay" />
                </div>
                <!-- <img src="assets/images/aboutus-header.png" class="headerImage" />
                <img src="assets/images/servicesOverlay.svg" class="servicesHeaderOverlay" /> -->

            </div>
        </div>
        <div class="aboutUsSectionTwo">
            <div class="row aboutUsleft" id="mission">
                <div class="pageTitle">
                    <div class="col-md-12">
                        <h4 class="serviceHeaderTitle">About UNILIGHT</h4>
                    </div>
                </div>

                <div class="col-md-5 aboutusLeftMain">
                    <div class="aleftBlock">
                        <div class="alertImg">
                            <img src="assets/images/aboutus-vision.svg" />
                        </div>
                        <h1>Our Vision</h1>
                        <p>Our Vision To be one of the most respected and credible Insurance Brokers through Fair,
                            Transparent, Competitive, Ethical and Expeditious Service.</p>
                    </div>
                    <hr />
                    <div class="aleftBlock">
                        <div class="alertImg">
                            <img src="assets/images/aboutus-mission.svg" />
                        </div>
                        <h1>Our Mission</h1>
                        <p>Our Mission To guide our customers transparently and ethically in their journey of buying
                            appropriate insurance covers. We will stay focused on delivering superior products and
                            excellent service to our customers and operate our business through the shared value of
                            trust, tolerance and openness.</p>
                    </div>

                </div>
                <div class="col-md-7">
                    <div class="aboutContent">
                        <p>Unilight is a home grown insurance & reinsurance broker duly licensed by IRDAI in 2013 . We
                            are one of the fastest growing brokerage house in India ,driven by a group of industry
                            professionals having expertise in providing clients with tailor made Insurance solutions
                            including claims management risk, audit etc. Unilight recognises value of blending youth
                            with
                            experience and have built a strong management team which has acumen, wisdom, experience,
                            future first attitude and ensures timely delivery of ‘real-time’ services.</p>
                        <p>The organisation practices sectoral expertise and each vertical is headed by a senior
                            executive. The segment specific team is equipped with deep understanding of their area of
                            expertise and help in providing the customers with an optimum insurance solution which is
                            commercially sound and competitively priced. Over the years, we have developed sector
                            specific skill sets in Manufacturing, Infrastructure ,Energy, , Ports & Terminals, Marine
                            Hull construction Jewellers and Diamond processing, Financial Institutions Group, Specie ,
                            Cash management services, Privilege Signature Services. </p>
                        <p>As a practice we reach out to Corporates and assist them in managing and mitigating their
                            risks. We are confident of our resources and capabilities to provide innovative solutions
                            and the best services at all times. All our skills and abilities are concentrated in
                            negotiating the most advantageous, commercially sound and competitively priced insurance
                            program for each of our Clients.</p>
                        <p>Our prime focus has been on client service and satisfaction. We lay a strong emphasis on
                            assigning the right service team members to each client, who would consider us as an
                            extension of their own risk management team. Our commitment to quality and integrity is
                            combined with our emphasis on speed and efficiency.
                        <p>
                        <p> Unilight believes investing in building long lasting relations with its customer driven
                            through our domain expertise and service commitment. Its unerring focus lies on de-risking
                            the unknown and unforeseen risks faced by its clients in their respective areas of business
                            and establish a responsible partnership which add and create value for all its stakeholders.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row csrSection" id="csrSection">
                <div class="container">
                    <div class="col-md-12">
                        <h1 class="csrSectionTitle">Our CSR Philosophy</h1>
                        <p>
                            We live a dynamic life in a world that is growing more and more complex. Global scale
                            environment, social, cultural and economic issues have now become part of our everyday life.
                            Boosting profits is no longer the sole business performance indicator for the corporate and
                            they have to play the role of responsible corporate citizens as they owe a duty towards the
                            society.
                        </p>
                        <br />

                        <div class="row">
                            <div class="col-md-8">
                                <div class="serviceHightlight">
                                    <p>
                                        "Sustainability, social equality and the environment are now business
                                        problems.<br />
                                        And corporate leaders can't depend on governments to solve them”.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <br />
                        <p>
                            At Unilight we believe that " Integrating social, environmental and ethical responsibilities
                            into the governance of businesses ensures long term success, competitiveness and
                            sustainability”.
                            <br />
                            <br />
                            Corporate Social Responsibility (CSR) is a continuous commitment of Unilight to improve
                            community well being, through ethical contribution of our resources

                        </p>


                        <div class="row serviceHightlight aboutUsHightligh">
                            <div class="col-md-6">
                                <h1>It is the core corporate responsibility of Unilight to the society to pursue
                                    its corporate value enhancement through promoting, developing and
                                    contributing in the following areas : </h1>
                                <ul>
                                    <li>Development of Education Infrastructure and promotion of Education.
                                        Construction of School Buildings with associated infrastructure in Rural
                                        areas, to ensure Quality Education for children in the remote Rural
                                        areas.
                                    </li>
                                    <li>Promoting, developing and contributing in the field of sports like
                                        Swimming, Fencing, Football, Cricket, Badminton, Paralympic sports etc.
                                        Organizing different Coaching camps for children.
                                    </li>
                                    <li>• Supporting the peoples affected by Cyclone, Flood and other Natural
                                        Disasters with supply of essential goods like polythene, food and
                                        medicines. Supporting them with financial assistance for rebuilding
                                        their homes damaged by these Natural Disasters.
                                    </li>
                                    <li>
                                        Development of Drinking water facilities at remote Rural areas to
                                        provide safe drinking water to people.

                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <h1></h1>
                                <br />
                                <br />
                                <br />
                                <br />
                                <ul>
                                    <li>Helping Handicapped people in Rural areas by providing them with
                                        specially designed scooters for commuting in rural areas. Helping
                                        handicapped people with Specially designed Electric wheelchairs for
                                        their movement.</li>
                                    <li>Helping the Rural Health sector by distributing basic medical
                                        diagnostic equipment in remote villages in Rural areas.
                                    </li>
                                    <li>
                                        Providing with financial assistance towards restoration of heritage
                                        sites, buildings of historical importance and works of art.
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="row" id="bodire">
                            <div class="col-md-10">
                                <h1 class="bodD">Board of Directors</h1>
                                <h2 class="bodD-sub">Unilight Insurance Brokers Private Limited</h2>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="bodContain">
                                            <div class="bodProfileImg">
                                                <img src="assets/images/male.webp" />
                                                <p>khushal Jhaveri <span>Director</span></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="bodContain">
                                            <div class="bodProfileImg">
                                                <img src="assets/images/male.webp" />
                                                <p>Jeegar Shah <span>Director & Principal Officer</span></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="bodContain">
                                            <div class="bodProfileImg">
                                                <img src="assets/images/female.webp" />
                                                <p>Naini Jhaveri <span>Director</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <div class="row">
                            <div class="col-md-10">
                                <h2 class="bodD-sub">Unilight Re-insurance Brokers Private Limited</h1>
                                    <!-- <h2>Unilight Insurance Brokers Private Limited</h2> -->

                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="bodContain">
                                                <div class="bodProfileImg">
                                                    <img src="assets/images/male.webp" />
                                                    <p>S. Narayanan <span>Chairman</span></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="bodContain">
                                                <div class="bodProfileImg">
                                                    <img src="assets/images/male.webp" />
                                                    <p>Biswajeet Mohanty <span>Director</span></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="bodContain">
                                                <div class="bodProfileImg">
                                                    <img src="assets/images/male.webp" />
                                                    <p>K.K. Aggarwal <span>Director – Underwriting / Claim</span></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="bodContain">
                                                <div class="bodProfileImg">
                                                    <img src="assets/images/male.webp" />
                                                    <p>Bhaw Dutt<span>Director Commercial Business</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <div class="row" id="managementTeam">
                            <div class="col-md-12">
                                <h2 class="bodD-sub">Core Team</h1>
                                    <div class="swiper mySwiperAbout">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide">
                                                <div class="bodContain">
                                                    <div class="bodProfileImg">
                                                        <img src="assets/images/female.webp" />
                                                        <p>Mamta Murarka <span>Marine and Port Business </span></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="bodContain">
                                                    <div class="bodProfileImg">
                                                        <img src="assets/images/male.webp" />
                                                        <p>Jagannath Gupta <span>Property & Casualty </span></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="bodContain">
                                                    <div class="bodProfileImg">
                                                        <img src="assets/images/male.webp" />
                                                        <p>S.Mohanty <span>PV and Liabilities </span></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="bodContain">
                                                    <div class="bodProfileImg">
                                                        <img src="assets/images/male.webp" />
                                                        <p>Rajguru Kadge <span>International Business</span></p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="swiper-slide">
                                                <div class="bodContain">
                                                    <div class="bodProfileImg">
                                                        <img src="assets/images/male.webp" />
                                                        <p>Neeraj Raj <span>Commercial Business - Mumbai </span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="swiper-slide">
                                                <div class="bodContain">
                                                    <div class="bodProfileImg">
                                                        <img src="assets/images/male.webp" />
                                                        <p>Vasant Dayama<span>Retail - Head</span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="swiper-slide">
                                                <div class="bodContain">
                                                    <div class="bodProfileImg">
                                                        <img src="assets/images/male.webp" />
                                                        <p>Priyadarshi Chowdhury <span>Commercial Business - Delhi
                                                            </span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="swiper-slide">
                                                <div class="bodContain">
                                                    <div class="bodProfileImg">
                                                        <img src="assets/images/male.webp" />
                                                        <p>Dhiraj Gupta <span>Retail Business - Delhi
                                                            </span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="swiper-slide">
                                                <div class="bodContain">
                                                    <div class="bodProfileImg">
                                                        <img src="assets/images/male.webp" />
                                                        <p>Amit Paul Chowdhury <span>Commercial Business - Kolkata
                                                            </span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="swiper-slide">
                                                <div class="bodContain">
                                                    <div class="bodProfileImg">
                                                        <img src="assets/images/male.webp" />
                                                        <p>Debabrata Mohanty<span> Retail Business - Bhubaneswar
                                                            </span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>


                                            <!-- <div class="swiper-slide">
                                                <div class="bodContain">
                                                    <div class="bodProfileImg">
                                                        <img src="assets/images/profiles/Rectangle 1.png" />
                                                        <p>Vasant Dayama <span>Head – Retail Lines </span></p>
                                                    </div>
                                                </div>
                                            </div>
                                            -->
                                        </div>
                                        <div class="swiper-pagination"></div>
                                    </div>
                            </div>
                        </div>

                        <br />
                        <br />
                    </div>
                </div>
            </div>
        </div>
        <div class="serSectionOne" style="padding-bottom:0">
            <div class="mainHeaderImage">
                <div class="parallax-window" style="min-height:400px" data-parallax="scroll"
                    data-image-src="assets/images/Rectangle 94.png">
                    <!-- <img src="assets/images/servicesOverlay.svg" class="servicesHeaderOverlay" /> -->
                </div>
                <div class="divOverlay"></div>
                <!-- <img src="assets/images/aboutus-header.png" class="headerImage" />
                <img src="assets/images/servicesOverlay.svg" class="servicesHeaderOverlay" /> -->
                <a href="contactus.php" class="serviceHeaderTitle" style="top:45%">Join Our Team <img
                        src="assets/images/ionic-ios-arrow-dropright-circle.svg" /></a>
            </div>
        </div>
    </main>
    <?php include 'common/footer.php'; ?>
</body>

</html>