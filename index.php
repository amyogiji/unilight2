<!DOCTYPE html>
<html lang="en">
<?php include 'common/head.php'; ?>

<body>
    <?php include 'common/header.php'; ?>

    <main class="sectionOne topPadding">
        <div class="bd-masthead mb-3">
            <img class="scrolldownimage bounce" src="assets/images/scrolldown.svg" />
            <div class="homeHead"></div>
            <div class="container no-padding">
                <div class="row">
                    <div class="col-md-8 col-lg-7 text-md-start mobileHomebg">
                        <div class="home-main-slider-connected wow fadeInLeft" data-wow-delay="0.5s">
                            <div class="headerLeftText">
                                <h1 class="mb-3">Comprehensive <br />risk transfer solutions</h1>
                                <p class="lead">Helping Businesses in implementing comprehensive <br /> insurance and
                                    reinsurance
                                    solutions</p>

                                <div class="d-flex flex-column flex-md-row">
                                    <a class="knowMore btn btn-lg cusbtn mb-3 me-md-3">Know More</a>
                                </div>
                            </div>
                            <div class="headerLeftText">
                                <h1 class="mb-3">Thoughtful Leadership</h1>
                                <p class="lead">Sectorial Experts to advise what is best for you</p>

                                <div class="d-flex ßflex-column flex-md-row">
                                    <a class="knowMore btn btn-lg cusbtn mb-3 me-md-3">Know More</a>
                                </div>
                            </div>
                            <div class="headerLeftText">
                                <h1 class="mb-3">Enabling Insurance</h1>
                                <p class="lead">Digital Innovations for creating convenience in buying insurance</p>

                                <div class="d-flex flex-column flex-md-row">
                                    <a class="knowMore btn btn-lg cusbtn mb-3 me-md-3">Know More</a>
                                </div>
                            </div>
                        </div>

                        <div class="mobileSlider">
                            <div class="home-main-slider2">
                                <img src="assets/images/banner1withshadow.png" alt="">
                                <img src="assets/images/slider2.png" alt="">
                                <img src="assets/images/banner3withshadow.png" alt="">
                            </div>
                        </div>
                        <div class="row row-cols-12 wow fadeInUp fourIconBox fourIconPadding" data-wow-delay="0.5s">
                            <div class="col-md-6 col-sm-12">
                                <div class="headerBottomCon wow bounceInUp">
                                    <div class="row row-cols-12">
                                        <div class="col-2"> <img src="assets/images/emplo.svg" /></div>
                                        <div class="col-10">
                                            <p>140 <span> No of employees</span></p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="headerBottomCon">
                                    <div class="row">
                                        <div class="col-2"> <img src="assets/images/noofoffice.svg" /></div>
                                        <div class="col-10">
                                            <p>8 <span>No of Offices</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row row-cols-12 wow fadeInUp fourIconBox fourIconPadding" data-wow-delay="0.5s">
                            <div class="col-md-6 col-sm-12">
                                <div class="headerBottomCon wow bounceInUp">
                                    <div class="row row-cols-12">
                                        <div class="col-2"> <img src="assets/images/noofclients.svg" /></div>
                                        <div class="col-10">
                                            <p>10000+ <span>No of clients</span></p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="headerBottomCon">
                                    <div class="row">
                                        <div class="col-2"> <img src="assets/images/premiumcon.svg" /></div>
                                        <div class="col-10">
                                            <p>400+ cr. <span> Premium Placement</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-8 mx-auto col-md-4 col-lg-5 wow fadeInRight" data-wow-delay="0.5s">
                        <div class="home-main-slider desktopHomeSlider">
                            <img src="assets/images/banner1withshadow.png" alt="">
                            <img src="assets/images/slider2.png" alt="">
                            <img src="assets/images/banner3withshadow.png" alt="">
                        </div>
                    </div>


                </div>
            </div>
        </div>
        </div>

    </main>


    <div class="sectionTwo wow fadeInUp" data-wow-delay="0.5s" id="moreServices">
        <div class="container-fuild">
            <div class="row align-items-lg-center">
                <div class="col-md-5 secTwoContent text-center text-md-start">
                    <img src="assets/images/mousehome.svg" />
                    <h1>Explore the wide range of our services</h1>
                    <p>Geared up to provide entire gamut of services ranging from insurance placement to Risk and claim
                        management.</p>
                    <a href="#" class="btn btn-lg cusbtn mb-3 me-md-3">Contact Our Advisor</a>
                </div>
                <div class="col-md-7">
                    <div class="service-slider">
                        <div class="sliderCont">
                            <div class="slImg">
                                <img src="assets/images/services/commercialinsurance.jpg" />
                                <div class="sloverlay"></div>
                            </div>
                            <div class="sliderTextCon">
                                <h1>Commercial Insurance</h1>
                                <p>Unilight specialises in catering and serving the insurance needs of large and medium
                                    sized corporate entities in the manufacturing and services sector. Our Corporate
                                    solution team comprises senior management and highly experienced insurance
                                    professionals
                                    capable to understand industry specific risks and provide solutions in line with the
                                    available insurance products in domestic and global insurance markets.</p>
                                <a href="commercial-insurance.php">Learn More</a>
                            </div>
                        </div>
                        <div class="sliderCont">
                            <div class="slImg">
                                <img src="assets/images/products/retailinsurance-11.jpg" />
                                <div class="sloverlay"></div>
                            </div>
                            <div class="sliderTextCon">
                                <h1>Retail Insurance</h1>
                                <p>
                                    Unilight integrated online business portal policyongo.com provides customers the
                                    ease of
                                    buying and our POSP partners ease of selling retail insurance products Motor,
                                    Health,
                                    Accident, Travel, Householders and Life on real time basis. Our focus in retail
                                    is to
                                    provide insurance protection to populace specially in tier B and C cities in
                                    India with
                                    affordable insurance plan on real time basis and also to provide support in the
                                    event of
                                    a loss claim.
                                </p>
                                <a href="retail-insurance.php">Learn More</a>
                            </div>
                        </div>
                        <div class="sliderCont">
                            <div class="slImg">
                                <img src="assets/images/services/reinsuranceinsurance.jpg" />
                                <div class="sloverlay"></div>
                            </div>
                            <div class="sliderTextCon">
                                <h1>Reinsurance</h1>
                                <p>
                                    Unilight has emerged as market leader in providing reinsurance support to domestic
                                    and
                                    international insurers both on Facultative and treaty arrangement basis. With the
                                    opening up of Insurance Market, India has witnessed entry of large number of private
                                    insurers and resultantly insurance market in terms of penetration and volumes has
                                    grown
                                    exponentially in last decade.
                                </p>
                                <a href="reinsurance-placement.php">Learn More</a>
                            </div>
                        </div>
                        <div class="sliderCont">
                            <div class="slImg">
                                <img src="assets/images/services/claimmanagement.jpg" />
                                <div class="sloverlay"></div>
                            </div>
                            <div class="sliderTextCon">
                                <h1>Claim Management</h1>
                                <p>Most business entities face significant challenges during the Claim
                                    Management process.
                                    Preparing and presenting a claim can be a complex and time-consuming process
                                    and may
                                    need support of an expert.
                                </p>
                            </div>
                            <a href="claim-management.php">Learn More</a>
                        </div>
                        <div class="sliderCont">
                            <div class="slImg">
                                <img src="assets/images/services/riskmanagement.jpg" />
                                <div class="sloverlay"></div>
                            </div>
                            <div class="sliderTextCon">
                                <h1>Risk Management</h1>
                                <p>Unilight offers end-to end risks mitigation solutions to our clients
                                    beginning with risks
                                    identification, risks recognition, risks assessment, peer comparison and
                                    risks
                                    mitigation solutions while keeping an eye on the regulations in force.
                                    Clients considers
                                    us as a valuable partners for their risk management process.

                                </p>
                                <a href="risk-management.php">Learn More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="sectionThree mobileSec">
        <div class="container-fluid no-padding">
            <div class="row">
                <div class="col-md-6 wow fadeInLeft" data-wow-delay="0.5s">
                    <img src="assets/images/Bitmap-4.png" class="homeLaptop" />
                </div>
                <div class="col-md-6 wow fadeInRight" data-wow-delay="0.5s">
                    <div class="sthreeRight">
                        <img src="assets/images/Asset 2.png" />
                        <h1>Creating convenience with great price <br /><span>Motor, Health and Life
                                Insurance</span>
                        </h1>
                        <p>Policyongo.com is a digital asset of Unilight Insurance Brokers for distribution of
                            retail
                            insurance products on-line to its customers and through POSP distribution model
                            insurance.
                        </p>
                        <a href="https://policyongo.com/" target="_blank" class="btn btn-lg cusbtn mb-3 me-md-3">Visit
                            Website</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <main>
        <div class="sectionFour contactForm  getInHomePage wow fadeInUp" data-wow-delay="0.5s" style="margin:0">
            <div class="container-fluid no-padding">
                <div class="row">
                    <?php include 'common/contactform.php'; ?>
                    <div class="googlemap no-padding getInAwards googlemapM">
                        <div class="row">
                            <div class="col-md-12">
                                <h1 class="gt-title">Unilight Awards & CSR</h1>
                                <p>Geared up to provide entire gamut of services ranging from insurance
                                    placement to Risk
                                    and claim management and claim management</p>
                            </div>
                        </div>
                        <div class="row row-cols-12 imgTop">

                            <div class="csr-slider">
                                <div class="imgCon">
                                    <div class="csrImage">
                                        <img src="assets/images/handi1.png" />
                                        <p class="awardsDesc">Helping Handicaps under CSR</p>
                                    </div>
                                </div>
                                <div class="imgCon">
                                    <img src="assets/images/handi2.png" />
                                    <p class="awardsDesc">Helping Handicaps under CSR</p>
                                </div>
                                <div class="imgCon">
                                    <img src="assets/images/cheque.png" />
                                    <p class="awardsDesc">Rakhal Shethi-Para Fencing champion</p>
                                </div>
                                <div class="imgCon">
                                    <img src="assets/images/dsc_1239-min2.jpeg" />
                                    <p class="awardsDesc">Inauguration of New School Building at Brahmagiri Odisha</p>
                                </div>
                                <div class="imgCon">
                                    <img src="assets/images/football1.jpg" />
                                    <p class="awardsDesc">Contribution for Interschool Football Cup Under CSR Program
                                    </p>
                                </div>

                            </div>
                        </div>

                        <div class="row imgTop">

                            <div class="csr-slider2">
                                <div class="imgCon">
                                    <img src="assets/images/bike.png" />
                                    <p class="awardsDesc">Unilight Cup Cricket Tournament</p>
                                </div>
                                <div class="imgCon">
                                    <img
                                        src="assets/images/Construction of School at Brahmagiri Odisha from CSR funds.JPG" />
                                    <p class="awardsDesc">Construction of School at Brahmagiri Odisha from CSR funds
                                    </p>
                                </div>
                                <div class="imgCon">
                                    <img src="assets/images/football2.jpg" />
                                    <p class="awardsDesc">Contribution for Interschool Football Cup Under CSR Program.
                                </div>
                                <div class="imgCon">
                                    <img src="assets/images/schoolchildren.jpg" />
                                    <p class="awardsDesc">Constructions of School at Brahmagiri Odisha Under CSR
                                        Program.
                                </div>
                                <div class="imgCon">
                                    <img src="assets/images/swimminggirl.jpg" />
                                    <p class="awardsDesc">Sponsorship for our national junior swimming competition
                                </div>
                            </div>
                            <!-- <div class="col-md-6">
                                <div class="imgCon">
                                    <img src="assets/images/bike.png" />
                                    <p class="awardsDesc">Unilight Cup Cricket Tournament</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="imgCon">
                                    <img
                                        src="assets/images/Construction of School at Brahmagiri Odisha from CSR funds.JPG" />
                                    <p class="awardsDesc">Construction of School at Brahmagiri Odisha from CSR funds
                                    </p>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </main>


    <?php include 'common/footer.php'; ?>

</body>

</html>