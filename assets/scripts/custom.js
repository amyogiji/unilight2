$(document).ready(function () {
    $('.home-main-slider').slick({
        asNavFor: '.home-main-slider-connected',
        fade: true,
        dots: true,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 3000
    });
    var homeMainSliderContainer = $('.home-main-slider');
    $(".home-main-slider").on("beforeChange", function (event, slick, currentSlide, nextSlide) {
        homeMainSliderContainer.find('.slick-dots li').removeClass('progress');
        homeMainSliderContainer.find('.slick-dots li').each(function (i, item) {
            // console.log(i, currentSlide)
            if (i <= nextSlide) {
                $(this).addClass('progress');
            }
        });
    })


    $('.csr-slider').slick({
        dots: true,
        arrows: false,
        slidesToShow: 3,
        infinite: false,
        freeMode: true,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 1.5,
                centerMode: false,
                /* set centerMode to false to show complete slide instead of 3 */
                slidesToScroll: 1
            }
        }]
    });


    $('.csr-slider2').slick({
        dots: true,
        arrows: false,
        slidesToShow: 3,
        infinite: false,
        freeMode: true,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 1.5,
                centerMode: false,
                /* set centerMode to false to show complete slide instead of 3 */
                slidesToScroll: 1
            }
        }]
    });



    $('.home-main-slider2').slick({
        // asNavFor: '.home-main-slider-connected',
        fade: true,
        dots: true,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 3000
    });
    var homeMainSliderContainer2 = $('.home-main-slider2');
    $(".home-main-slider2").on("beforeChange", function (event, slick, currentSlide, nextSlide) {
        homeMainSliderContainer2.find('.slick-dots li').removeClass('progress');
        homeMainSliderContainer2.find('.slick-dots li').each(function (i, item) {
            // console.log(i, currentSlide)
            if (i <= nextSlide) {
                $(this).addClass('progress');
            }
        });
    })

    $('.home-main-slider-connected').slick({
        draggable: false,
        touchMove: false,
        arrows: false
    });


});