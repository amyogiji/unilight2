<!DOCTYPE html>
<html lang="en">
<?php include 'common/head.php'; ?>

<body>
    <?php include 'common/header.php'; ?>
    <main>
        <div class="serSectionOne productServices">
            <div class="mainHeaderImage">
                <div class="parallax-window" data-parallax="scroll"
                    data-image-src="assets/images/products/newproject.jpg">
                </div>

                <!-- <img src="assets/images/products-marine.png" class="headerImage" /> -->
                <div class="divOverlay"></div>
                <!-- <img src="assets/images/servicesOverlay.svg" class="servicesHeaderOverlay"/> -->
                <!-- <h1 class="serviceHeaderTitle">Employee's Benefit</h1> -->
            </div>
        </div>

        <div class="serSectionTwo">
            <div class="container-fuild">

                <div class="row">
                    <div class="col-md-9">
                        <div class="container">
                            <div class="row productServiceleft">
                                <div class="col-md-12">
                                    <h4 class="serviceHeaderTitle">Employee's Benefit</h4>
                                    <br />
                                    <ul class="breadcrumb">
                                        <li><a href="/">Home ></a></li>
                                        <li><a href="#">Products ></a></li>
                                        <li><a href="employee-insurance.php">Employee's Benefit</a></li>
                                    </ul>
                                    <p class="productSerhead">
                                        Employees are most valuable and complex assets for any business entity thus
                                        required to be taken care specially when they are in crises following an adverse
                                        event. With this in mind most of the businesses offer their employees insurance
                                        benefits to stay them comfortable. This also works as a talent retention
                                        strategy and needs to be in line with the competition. Followings are the most
                                        common form of employees benefit insurance policies being offered by insurers in
                                        India:
                                    </p>
                                    <br />
                                    <div class="accordion custAccordion" id="accordionExample">
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="headingOne">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#collapseOne" aria-expanded="true"
                                                    aria-controls="collapseOne">
                                                    Group Health / Medical insurance
                                                </button>
                                            </h2>
                                            <div id="collapseOne" class="accordion-collapse collapse show"
                                                aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    Group Health / Medical Insurance (GMC) Policies are granted to
                                                    homogeneous Group of individual having some common relationship like
                                                    employer-employee, members of clubs/associations/societies,
                                                    depositors or borrowers of Banks/NBFC etc. Policy covers expenses
                                                    for Hospitalization and Domiciliary Hospitalization incurred for
                                                    treatment of physical disease or injury sustained/incepted during
                                                    the Policy Period Domiciliary Hospitalization expenses relates to
                                                    costs of medical treatment which would normally require hospital
                                                    admission, but is actually taken at home for specified reasons.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="headingTwo">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#collapseTwo" aria-expanded="true"
                                                    aria-controls="collapseTwo">
                                                    Group Personal Accident Insurance
                                                </button>
                                            </h2>
                                            <div id="collapseTwo" class="accordion-collapse collapse"
                                                aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    Group Personal Accident Insurance provides cover to the employee /
                                                    employee’s family in the event of death or defined disablement due
                                                    to an accident occurring during the course of employment. Benefits
                                                    payable under the Policy are a lump-sum capital benefit for Death,
                                                    Loss of Limbs, Permanent and weekly benefit for Temporary
                                                    Disabilities following an accident occurrence.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="headingThree">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#collapseThree" aria-expanded="true"
                                                    aria-controls="collapseThree">
                                                    Group Term Life Insurance Policy
                                                </button>
                                            </h2>
                                            <div id="collapseThree" class="accordion-collapse collapse"
                                                aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    Group Term Insurance Policy is meant to provide financial protection
                                                    to the employee’s family in the event of his/ her death. This policy
                                                    requires only one master policy to employer covering multiple
                                                    individual employees as beneficiaries with option of varied Sum
                                                    Insured. In group term insurance risk assessment is done on whole of
                                                    the group rather than for the risk factors on an individual basis.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="headingFour">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#collapseFour" aria-expanded="true"
                                                    aria-controls="collapseFour">
                                                    Gratuity
                                                </button>
                                            </h2>
                                            <div id="collapseFour" class="accordion-collapse collapse"
                                                aria-labelledby="headingFour" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    Gratuity is a defined benefit plan and is one of the many retirement
                                                    benefits offered by the employer to the employee upon leaving his
                                                    job when an employee completes 5 or more years of full time service
                                                    with the employer.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="serviceHightlight">
                                <h1>How are we at Unilight different?</h1>
                                <ul>
                                    <li>How are we at Unilight different? Team of experts with understanding of practices, risks, exposures across industries.</li>
                                    <li>Ability to carry out risk inspections, safety audits and project risk consulting for identification of risks and offering Loss Prevention solutions.</li>
                                    <li>Provide Innovative Insurance Solutions that help in risk mitigation and risk management.</li>
                                    <li>Technical Assistance in communicating Industrial Risk-related information and Risk Reduction/Mitigating Measures to your company’s board, technical committees and/or the (re-) insurance industry.</li>                                   
                                </ul>
                            </div> -->
                    </div>
                    <div class="col-md-3">
                        <?php include 'common/products/products-right.php' ?>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php include 'common/footer.php'; ?>
</body>

</html>