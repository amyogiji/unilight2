<!DOCTYPE html>
<html lang="en">
<?php include 'common/head.php'; ?>

<body>
    <?php include 'common/header.php'; ?>
    <main>
        <div class="serSectionOne productServices">
            <div class="mainHeaderImage">
                <div class="parallax-window" data-parallax="scroll"
                    data-image-src="assets/images/services/commercialinsurance.jpg">
                    <img src="assets/images/servicesOverlay.svg" class="servicesHeaderOverlay" />
                </div>
                <!-- <img src="assets/images/services-header.png" class="headerImage" /> -->
                <!-- <h1 class="serviceHeaderTitle">Commercial Insurance</h1> -->
            </div>
        </div>

        <div class="serSectionTwo">
            <div class="container-fuild">
                <div class="row">
                    <div class="col-md-9">
                        <div class="container">
                            <div class="row productServiceleft">
                                <div class="col-md-12">
                                    <h4 class="serviceHeaderTitle">Commercial Insurance</h4>
                                    <br />
                                    <ul class="breadcrumb">
                                        <li><a href="/">Home ></a></li>
                                        <li><a href="#">Services ></a></li>
                                        <li><a href="commercial-insurance.php">Commercial Insurance</a></li>
                                    </ul>
                                    <p class="serSTOne">Unilight specialises in catering and serving the insurance needs
                                        of large
                                        and medium sized corporate entities in the manufacturing and services sector.
                                        Our Corporate
                                        solution team comprises senior management and highly experienced insurance
                                        professionals
                                        capable to understand industry specific risks and provide solutions in line with
                                        the
                                        available insurance products in domestic and global insurance markets.</p>
                                    <br />
                                    <p>Unilight specialises in catering and serving the insurance needs of large and
                                        medium sized
                                        corporate entities in the manufacturing and services sector. Our Corporate
                                        solution team
                                        comprises senior management and highly experienced insurance professionals
                                        capable to
                                        understand industry specific risks and provide solutions in line with the
                                        available
                                        insurance products in domestic and global insurance markets.</p>

                                    <div class="serviceHightlight">
                                        <h1>How are we at Unilight different?</h1>
                                        <ul>
                                            <li>Team of Domain and sectoral experts with high level of service
                                                commitment. </li>
                                            <li>Ability to provide end-to end solutions to customers in their risk
                                                management and
                                                insurance needs. </li>
                                            <li>Exposure to global insurance markets and practise backed with
                                                reinsurance placement
                                                ability. </li>
                                            <li>Support in Claims Management.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />
                    </div>
                    <div class="col-md-3">
                        <?php include 'common/services/services-right.php' ?>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php include 'common/footer.php'; ?>
</body>

</html>