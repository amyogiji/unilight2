<!DOCTYPE html>
<html lang="en">
<?php include 'common/head.php'; ?>

<body>
    <?php include 'common/header.php'; ?>
    <main>
        <div class="serSectionOne productServices">
            <div class="mainHeaderImage">
                <div class="parallax-window" data-parallax="scroll"
                    data-image-src="assets/images/services/reinsuranceinsurance.jpg">
                    <img src="assets/images/servicesOverlay.svg" class="servicesHeaderOverlay" />
                </div>
                <!-- <img src="assets/images/service-reinsurance.png" class="headerImage" /> -->
                <!-- <h1 class="serviceHeaderTitle">Reinsurance Insurance</h1> -->
            </div>
        </div>

        <div class="serSectionTwo">
            <div class="container-fuild">
                <div class="row">
                    <div class="col-md-9">
                        <div class="container">
                            <div class="row productServiceleft">
                                <div class="col-md-12">
                                    <h4 class="serviceHeaderTitle">Reinsurance Insurance</h4>
                                    <br />
                                    <ul class="breadcrumb">
                                        <li><a href="/">Home ></a></li>
                                        <li><a href="#">Services ></a></li>
                                        <li><a href="reinsurance-placement.php">Reinsurance Insurance</a></li>
                                    </ul>
                                    <p class="serSTOne">
                                        Unilight has emerged as market leader in providing reinsurance support to
                                        domestic and
                                        international insurers both on Facultative and treaty arrangement basis. With
                                        the opening up
                                        of Insurance Market, India has witnessed entry of large number of private
                                        insurers and
                                        resultantly insurance market in terms of penetration and volumes has grown
                                        exponentially in
                                        last decade. Rapid industrialisation and focus on infrastructure development
                                        have seen risks
                                        growing in size and complexity. New aged service sector and E-commerce business
                                        has
                                        redefined business models and exposure to business risk. This economic
                                        environment change
                                        has resulted into increased demand of insurance protection both for insured and
                                        insures end.
                                    </p>
                                    <br />
                                    <p>
                                        We are committed to assist our cedants for their requirements across all Lines
                                        of Business
                                        but with a focus on the following lines.
                                    </p>
                                    <br />
                                    <div class="serviceHightlight">
                                        <h1>How are we at Unilight different?</h1>
                                        <ul>
                                            <li>How are we at Unilight different? Our understanding of risks and ability
                                                to
                                                structure reinsurance programme.</li>
                                            <li>Knowledge and ability to reach specialised reinsurance markets.</li>
                                            <li>Credibility with reinsures and ability to deliver on-time facultative
                                                reinsurance
                                                support to our cedants at fair cost.</li>
                                            <li>Post placement support in claim follow up and recoveries.</li>
                                        </ul>
                                    </div>
                                    <br />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <?php include 'common/services/services-right.php' ?>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php include 'common/footer.php'; ?>
</body>

</html>