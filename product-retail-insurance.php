<!DOCTYPE html>
<html lang="en">
<?php include 'common/head.php'; ?>

<body>
    <?php include 'common/header.php'; ?>
    <main>
        <div class="serSectionOne productServices">
            <div class="mainHeaderImage">
                <div class="parallax-window" data-parallax="scroll"
                    data-image-src="assets/images/products/retailinsurance-1.jpg">
                </div>

                <!-- <img src="assets/images/products-marine.png" class="headerImage" /> -->
                <div class="divOverlay"></div>
                <!-- <img src="assets/images/servicesOverlay.svg" class="servicesHeaderOverlay"/> -->
                <!-- <h1 class="serviceHeaderTitle">Retail Insurance</h1> -->
            </div>
        </div>

        <div class="serSectionTwo">
            <div class="container-fuild">

                <div class="row">
                    <div class="col-md-9">
                        <div class="container">
                            <div class="row productServiceleft">
                                <div class="col-md-12">
                                    <h4 class="serviceHeaderTitle">Retail Insurance</h4>
                                    <br />
                                    <ul class="breadcrumb">
                                        <li><a href="/">Home ></a></li>
                                        <li><a href="#">Products ></a></li>
                                        <li><a href="product-retail-insurance.php">Retail Insurance</a></li>
                                    </ul>
                                    <p class="productSerhead">
                                        General Insurance Policies generally meant for individuals and meant for mass
                                        distribution are classified as retail insurance. Followings are the most common
                                        form of retail insurance policies being offered by insurers in India:
                                    </p>

                                    <br />
                                    <ul>
                                        <li> Motor Insurance</li>
                                        <li>Health Insurance</li>
                                        <li>Travel Insurance</li>
                                        <li>Personal Accident Insurance</li>
                                        <li>HNI PCG Insurance</li>
                                    </ul>

                                    <!-- <div class="accordion custAccordion" id="accordionExample">
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="headingOne">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#collapseOne" aria-expanded="true"
                                                    aria-controls="collapseOne">
                                                    Standard Fire and Special Perils Policy
                                                </button>
                                            </h2>
                                            <div id="collapseOne" class="accordion-collapse collapse show"
                                                aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    Standard Fire and Special Perils Policy is the most widely used
                                                    named peril cover for all categories of Insureds to cover their
                                                    buildings and contents, including plant and machinery, electrical
                                                    installations, furniture, fixtures and fittings, as well as stocks
                                                    of all types, including goods held in trust or commission. Coverage
                                                    is on Reinstatement Value (new for old) basis except for stock which
                                                    is on Market Value basis. Stocks can be insured on Declaration
                                                    and/or Floater cover facilities
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="headingTwo">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#collapseTwo" aria-expanded="true"
                                                    aria-controls="collapseTwo">
                                                    Industrial All Risk Insurance
                                                </button>
                                            </h2>
                                            <div id="collapseTwo" class="accordion-collapse collapse"
                                                aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    Industrial All Risk (IAR) Policy is an exclusion based comprehensive
                                                    package cover, issued on ‘all risk’ basis covering Property damages,
                                                    Machinery Break down and Business interruptions under a single
                                                    policy. It overcomes a number of major constraints of the
                                                    conventional named covers in Standard Fire and Special Perils Policy
                                                    and MBD Policy.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="headingThree">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#collapseThree" aria-expanded="true"
                                                    aria-controls="collapseThree">
                                                    Mega Risk Insurance
                                                </button>
                                            </h2>
                                            <div id="collapseThree" class="accordion-collapse collapse"
                                                aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    Mega Risk Insurance Policy is the widest form of insurance cover,
                                                    suitable for very large manufacturing risks with sum insured
                                                    exceeding Rs 2500 Crores in a single location such as
                                                    petrochemicals, power, steel, oil, gas and fertilizer plants, which
                                                    may have requirement for customised insurance covers. It covers all
                                                    buildings, permanent and temporary installations, mechanical,
                                                    electrical and electronic equipments and all other contents,
                                                    including own stock in trade, goods held in trust etc.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="headingFour">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#collapseFour" aria-expanded="true"
                                                    aria-controls="collapseFour">
                                                    Machinery Breakdown Insurance
                                                </button>
                                            </h2>
                                            <div id="collapseFour" class="accordion-collapse collapse"
                                                aria-labelledby="headingFour" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    Commercial enterprises (particularly manufacturing units) often
                                                    experience breakdown of plant and machineries, which require their
                                                    repair or replacement. Machinery Breakdown Insurance Policy covers
                                                    losses due to such internal breakdowns to mechanical and electrical
                                                    equipment. Under this Policy, plant and machineries have to be
                                                    compulsorily insured on Reinstatement Value basis.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="heading5">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#collapseFour5" aria-expanded="true"
                                                    aria-controls="collapseFour">
                                                    Business Interruption Insurance
                                                </button>
                                            </h2>
                                            <div id="collapseFour5" class="accordion-collapse collapse"
                                                aria-labelledby="heading5" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    Business Interruption Insurance Policy meant to cover loss of Gross
                                                    Profit on account of reduction in turnover or output, following
                                                    damage to the insured property due to operation of any insured peril
                                                    under the corresponding Material Damage Policy. The Policy also pays
                                                    for additional expenditure towards Increased Cost of Working
                                                    incurred by the Insured, for utilising alternative sources and
                                                    avenues to reduce the interruption effect.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="heading6">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#collapseFour6" aria-expanded="true"
                                                    aria-controls="collapseFour">
                                                    Ports and Terminal Insurance
                                                </button>
                                            </h2>
                                            <div id="collapseFour6" class="accordion-collapse collapse"
                                                aria-labelledby="heading6" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    The litigious landscape, developed legal frameworks, varied vessel
                                                    and cargo interest around the world necessitated Port and terminal
                                                    operators to look for customized comprehensive insurance solutions.
                                                    Port and Terminal Insurance is an exclusion based all risk insurance
                                                    policy covering port and terminal risks in one single policy.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="heading7">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#collapseFour7" aria-expanded="true"
                                                    aria-controls="collapseFour">
                                                    Stand Alone Terrorism & Political Violence insurance
                                                </button>
                                            </h2>
                                            <div id="collapseFour7" class="accordion-collapse collapse"
                                                aria-labelledby="heading7" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    Increasing incidences of terrorism poses unique threats not only to
                                                    human lives but to corporates assets. Terrorism and political
                                                    violence insurance is meant to take care of such risks either as an
                                                    add-on cover in property insurance policy or a customized and cost
                                                    effective Stand Alone Terrorism & Political Violence Insurance
                                                    Policy.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="heading8">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#collapseFour9" aria-expanded="true"
                                                    aria-controls="collapseFour">
                                                    Boiler & Pressure Plant Insurance
                                                </button>
                                            </h2>
                                            <div id="collapseFour9" class="accordion-collapse collapse"
                                                aria-labelledby="heading8" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    Any damage to Boilers and pressure plants at manufacturing units can
                                                    cause these to explode or collapse with resultant catastrophic
                                                    proportions by damaging surrounding property of the Insured and/or
                                                    others. The Boiler & Pressure Plant Insurance Policy covers all such
                                                    risks.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="heading9">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#collapseFour10" aria-expanded="true"
                                                    aria-controls="collapseFour">
                                                    Burglary Insurance
                                                </button>
                                            </h2>
                                            <div id="collapseFour10" class="accordion-collapse collapse"
                                                aria-labelledby="heading9" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    Business premises (offices, shops, godowns or factories) are often
                                                    at risk of having a break-in or burglary in the premises, causing a
                                                    huge financial loss to the Insured. Burglary Policy provides
                                                    protection against such risks.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="heading10">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#collapseFour11" aria-expanded="true"
                                                    aria-controls="collapseFour">
                                                    Burglary Insurance
                                                </button>
                                            </h2>
                                            <div id="collapseFour11" class="accordion-collapse collapse"
                                                aria-labelledby="heading10" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    Business premises (offices, shops, godowns or factories) are often
                                                    at risk of having a break-in or burglary in the premises, causing a
                                                    huge financial loss to the Insured. Burglary Policy provides
                                                    protection against such risks.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="heading11">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#collapseFour12" aria-expanded="true"
                                                    aria-controls="collapseFour">
                                                    Electronic Equipment Insurance
                                                </button>
                                            </h2>
                                            <div id="collapseFour12" class="accordion-collapse collapse"
                                                aria-labelledby="heading11" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    Modern-age businesses have sizeable and cost heavy electronic
                                                    equipment’s. Electronic Equipment Insurance Policy covers damages to
                                                    various types of electronic equipment’s like computers,
                                                    microprocessors,
                                                    telecom equipment’s and medical/diagnostic equipment’s. Electronic
                                                    Equipment Insurance is an all risk policy covering cost of repair or
                                                    replacement arising out of damages to electronic
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="heading12">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#collapseFour13" aria-expanded="true"
                                                    aria-controls="collapseFour">
                                                    Contractor’s Plant & Machinery Insurance
                                                </button>
                                            </h2>
                                            <div id="collapseFour13" class="accordion-collapse collapse"
                                                aria-labelledby="heading12" data-bs-parent="#accordionExample">
                                                <div class="accordion-body">
                                                    Construction project sites and other site involving work of material
                                                    handling require deployment of different categories of mobile
                                                    equipment’s such as bulldozers, cranes, excavators, dumpers, mixing
                                                    plants, pavers, tunnel boring and drilling machines, pile driving
                                                    equipment’s etc. The Contractor’s Plant & Machinery Insurance Policy
                                                    provides comprehensive coverage against damages to these
                                                    equipment’s.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="heading13">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#collapseFour14" aria-expanded="true"
                                                    aria-controls="collapseFour">
                                                    Contractors All Risk Insurance Policy
                                                </button>
                                            </h2>
                                            <div id="collapseFour14" class="accordion-collapse collapse"
                                                aria-labelledby="heading13" data-bs-parent="#accordionExample">
                                                Infrastructure development like construction of bridges, dams, roads,
                                                tunnels, ports and terminals, metro and other similar infrastructure
                                                facilities are exposed to various risk during construction period..
                                                Contractor’s All Risk (CAR) Insurance Policy protects these different
                                                categories of civil engineering projects against various types of losses
                                                during this period.
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="heading14">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#collapseFour15" aria-expanded="true"
                                                    aria-controls="collapseFour">
                                                    Erection All Risk (EAR) Insurance Policy
                                                </button>
                                            </h2>
                                            <div id="collapseFour15" class="accordion-collapse collapse"
                                                aria-labelledby="heading14" data-bs-parent="#accordionExample">
                                                The Erection All Risk (EAR) Insurance Policy protects all categories of
                                                mechanical engineering projects (involving construction of factories and
                                                installation of plants and machineries) against various types of losses
                                                during the erection and testing periods.
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <h2 class="accordion-header" id="heading14">
                                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#collapseFour16" aria-expanded="true"
                                                    aria-controls="collapseFour">
                                                    Advance Loss of Profit (ALOP) Insurance Policy & Marine Delay in
                                                    Start Up
                                                </button>
                                            </h2>
                                            <div id="collapseFour16" class="accordion-collapse collapse"
                                                aria-labelledby="heading14" data-bs-parent="#accordionExample">
                                                Advance Loss of Profit (ALOP) Policy provides protection against
                                                potential financial impact due to delay in project completion on account
                                                of loss or damage during the storage, handling, erection and testing
                                                phases of a civil or mechanical engineering project. ALOP Cover operate
                                                concurrently with Material Damage cover in place, through a CAR or EAR
                                                Policy
                                            </div>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                        <!-- <div class="serviceHightlight">
                                <h1>How are we at Unilight different?</h1>
                                <ul>
                                    <li>How are we at Unilight different? Team of experts with understanding of practices, risks, exposures across industries.</li>
                                    <li>Ability to carry out risk inspections, safety audits and project risk consulting for identification of risks and offering Loss Prevention solutions.</li>
                                    <li>Provide Innovative Insurance Solutions that help in risk mitigation and risk management.</li>
                                    <li>Technical Assistance in communicating Industrial Risk-related information and Risk Reduction/Mitigating Measures to your company’s board, technical committees and/or the (re-) insurance industry.</li>                                   
                                </ul>
                            </div> -->
                    </div>
                    <div class="col-md-3">
                        <?php include 'common/products/products-right.php' ?>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php include 'common/footer.php'; ?>
</body>

</html>